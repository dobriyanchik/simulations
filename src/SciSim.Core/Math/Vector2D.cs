﻿namespace SciSim.Core.Math;

public class Vector2D
{
    public Vector2D(double x, double y)
    {
        X = x;
        Y = y;
    }

    public Vector2D() : this(0, 0)
    {
    }

    public static Vector2D NaN { get; } = new(double.NaN, double.NaN);

    public double X { get; set; }

    public double Y { get; set; }

    public static Vector2D operator +(Vector2D v1, Vector2D v2) => 
        new(v1.X + v2.X, v1.Y + v2.Y);

    public static Vector2D operator -(Vector2D v1, Vector2D v2) => 
        new(v1.X - v2.X, v1.Y - v2.Y);

    public static Vector2D operator *(Vector2D v, double num) => 
        new(v.X * num, v.Y * num);

    public static Vector2D operator /(Vector2D v, double num) => 
        new(v.X / num, v.Y / num);

    public override string ToString() =>
        $"[{X},{Y}]";

    public string ToString(string format) =>
        $"[{X.ToString(format)},{Y.ToString(format)}]";
}
