﻿namespace SciSim.Core.Math;

public static class Adjustment
{
    public static double NormalizeValue(double value, double maxValue)
    {
        double abs = System.Math.Abs(value);

        if (abs > maxValue)
        {
            double sign = System.Math.Sign(value);
            value = sign * (abs - (System.Math.Floor(abs / maxValue) * maxValue));
        }

        return value;
    }

    public static double AvoidNaNAndInfinity(double value) =>
        double.IsNaN(value) || double.IsInfinity(value) ? 0 : value;
}
