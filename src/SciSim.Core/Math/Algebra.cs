﻿namespace SciSim.Core.Math;

public static class Algebra
{
    public static double Module(double value1, double value2) =>
        System.Math.Sqrt(value1 * value1 + value2 * value2);

    /// <summary>
    /// Gets a value that does not change during the transfer and rotation of the coordinate axes, 
    /// but changes its sign when replacing the direction of one axis to the opposite
    /// </summary>
    /// <param name="x1"></param>
    /// <param name="y1"></param>
    /// <param name="x2"></param>
    /// <param name="y2"></param>
    /// <returns></returns>
    public static double Determinant(double x1, double y1, double x2, double y2) =>
        x1 * y2 - y1 * x2;

    public static double Determinant(Vector2D v1, Vector2D v2) =>
        v1.X * v2.Y - v1.Y * v2.X;

    public static double DotProduct(double x1, double y1, double x2, double y2) =>
        x1 * x2 + y1 * y2;

    public static double DotProduct(Vector2D v1, Vector2D v2) =>
        v1.X * v2.X + v1.Y * v2.Y;

    public static double Angle(double x1, double y1, double x2, double y2) =>
        System.Math.Atan2(Determinant(x1, y1, x2, y2), DotProduct(x1, y1, x2, y2));

    public static double Angle(Vector2D v1, Vector2D v2) =>
        System.Math.Atan2(Determinant(v1, v2), DotProduct(v1, v2));

    public static double Distance(double x1, double y1, double x2, double y2) =>
        Module(x1 - x2, y1 - y2);

    public static double Distance(Vector2D v1, Vector2D v2) =>
        Module(v1.X - v2.X, v1.Y - v2.Y);

    public static double CosTheta(Vector2D v1, Vector2D v2) =>
        CosTheta(v1.X, v1.Y, v2.X, v2.Y);

    /// <summary>
    /// The Cos theta or cos θ is the ratio of the adjacent side to the hypotenuse.
    /// </summary>
    /// <param name="x1"></param>
    /// <param name="y1"></param>
    /// <param name="x2"></param>
    /// <param name="y2"></param>
    /// <returns></returns>
    public static double CosTheta(double x1, double y1, double x2, double y2)
    {
        double v1 = Module(x1, y1);
        double v2 = Module(x2, y2);

        if (v1 == 0)
        {
            v1 = 1E-5;
        }

        if (v2 == 0)
        {
            v2 = 1E-5;
        }

        double cosTeta = (x1 * x2 + y1 * y2) / (v1 * v2);

        return cosTeta;
    }
}
