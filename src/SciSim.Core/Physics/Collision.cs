﻿using SciSim.Core.Math;

namespace SciSim.Core.Physics;

public class Collision
{
    private double severity = 0;

    public bool Collide { get; set; }

    public Vector2D Location { get; set; } = new (double.NaN, double.NaN);

    public double Severity
    {
        get => severity;

        set
        {
            severity = value;
            Collide = severity == 0 ? false : true;
        }
    }

    public override string ToString() =>
        $"{Location}>{Severity:F0}x";

    public string ToString(string format) =>
        $"{Location.ToString(format)}>{Severity:F2}x";
}
