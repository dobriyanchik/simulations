﻿namespace SciSim.Core.Physics;

public abstract class CollidingGeometry<T>
{
    public Collision Collision { get; set; } = new();

    public abstract bool CollidesWithAny(params T[] geometries);
}
