﻿using SciSim.Core.Math;

namespace SciSim.Core.Physics;

public class CollidingSegment : CollidingGeometry<CollidingSegment>
{
    public CollidingSegment() 
    {
        V1 = new Vector2D();
        V2 = new Vector2D();
    }

    public CollidingSegment(double x1, double y1, double x2, double y2)
    {
        V1 = new Vector2D(x1, y1);
        V2 = new Vector2D(x2, y2);
        MakeOrthogonalCorrection();
    }

    public CollidingSegment(Point point1, Point point2)
        : this(point1.X, point1.Y, point2.X, point2.Y)
    {
    }

    public Vector2D V1 { get; set; }

    public Vector2D V2 { get; set; }

    //public static CollidingSegment FromPoints(Point point1, Point point2)
    //{
    //    var segment = new CollidingSegment(point1.X, point1.Y, point2.X, point2.Y);
    //    segment.MakeOrthogonalCorrection();
    //    return segment;
    //}

    public override bool CollidesWithAny(params CollidingSegment[] segments)
    {
        MakeOrthogonalCorrection();

        double x1SubX2 = V1.X - V2.X;
        double y1SubY2 = V1.Y - V2.Y;
        double x1MulY2SubY1MulX2 = Algebra.Determinant(V1, V2);

        foreach (var segment in segments)
        {
            double x3x4Sub = segment.V1.X - segment.V2.X;
            double y3y4Sub = segment.V1.Y - segment.V2.Y;
            double x3MulY4SubY3MulX4 = Algebra.Determinant(segment.V1, segment.V2);
            double divider = x1SubX2 * y3y4Sub - y1SubY2 * x3x4Sub;

            if (divider != 0)
            {
                double x = (x1MulY2SubY1MulX2 * x3x4Sub - x1SubX2 * x3MulY4SubY3MulX4) / divider;
                double y = (x1MulY2SubY1MulX2 * y3y4Sub - y1SubY2 * x3MulY4SubY3MulX4) / divider;
                Vector2D cross = new(x, y);

                if (HasPoint(cross) && segment.HasPoint(cross))
                {
                    Collision.Location = cross;
                    return true;
                }
            }
        }

        Collision.Location = Vector2D.NaN;
        return false;
    }

    public bool HasPoint(double x, double y)
    {
        bool hasPoint;

        hasPoint = V1.X < V2.X ?
            x > V1.X && x < V2.X :
            x > V2.X && x < V1.X;

        hasPoint &= V1.Y < V2.Y ?
            y > V1.Y && y < V2.Y :
            y > V2.Y && y < V1.Y;

        return hasPoint;
    }

    public bool HasPoint(Vector2D pt) => HasPoint(pt.X, pt.Y);

    /// <summary>
    /// To get collision equation working, lines should not be totally vertical or totally horizontal.
    /// </summary>
    public void MakeOrthogonalCorrection()
    {
        if (V2.X == V1.X)
        {
            V2.X += 0.1;
        }

        if (V2.Y == V1.Y)
        {
            V2.Y += 0.1;
        }
    }
}
