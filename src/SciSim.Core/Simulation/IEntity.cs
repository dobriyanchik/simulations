﻿using SciSim.Core.Math;

namespace SciSim.Core.Simulation;

public interface IEntity
{
    Vector2D Location { get; set; }

    void InteractWith<T>(T environment) where T : Environment<T>;
}
