﻿using SciSim.Core.Math;

namespace SciSim.Core.Simulation;

public interface IMovingEntity : IEntity
{
    Vector2D MoveDirection { get; }

    double Angle { get; set; }

    double Speed { get; set; }

    void Move();
}
