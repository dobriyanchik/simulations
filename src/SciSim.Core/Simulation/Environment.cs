﻿using SciSim.Core.Math;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace SciSim.Core.Simulation;

public abstract class Environment<T> where T : Environment<T>
{
    protected Environment(int width, int height)
    {
        Width = width;
        Height = height;
        Entities = new List<IEntity>();
    }

    public int Width { get; set; }

    public int Height { get; set; }

    // Was LinkedList to avoid concurrent modification exception.
    public List<IEntity> Entities { get; }

    public delegate void EvolutionEnvironmentEvent(Environment<T> environment);

    public event EvolutionEnvironmentEvent OnUpdate;

    [MethodImpl(MethodImplOptions.Synchronized)]
    public void Iterate()
    {
        TimeStep();
        OnUpdate?.Invoke(this);
    }

    [MethodImpl(MethodImplOptions.Synchronized)]
    public void AddEntity(IEntity entity) =>
        Entities.Add(entity);

    [MethodImpl(MethodImplOptions.Synchronized)]
    public void RemoveEntity(IEntity entity) =>
        Entities.Remove(entity);

    public List<U> FilterBy<U>() where U : IEntity =>
        Entities
        .Where(e => e is U)
        .Select(e => (U)e)
        .ToList();

    public bool IsInSight(IMovingEntity movable, IEntity entity) =>
        Algebra.CosTheta(movable.MoveDirection, entity.Location - movable.Location) > 0;

    public double DistanceBetween(IEntity entity1, IEntity entity2) =>
        Algebra.Distance(entity1.Location, entity2.Location);

    protected abstract void TimeStep();

    /// <summary>
    /// Avoids moving outside of environment
    /// </summary>
    /// <param name="entity">target entity</param>
    protected void AvoidMovingOutsideOfBounds(IEntity entity)
    {
        double newX = entity.Location.X;
        double newY = entity.Location.Y;

        if (newX < 0)
        {
            entity.Location.X = 0;
        }
        else if (newX >= Width)
        {
            entity.Location.X = Width - 1;
        }

        if (newY < 0)
        {
            entity.Location.Y = 0;
        }
        else if (newY >= Height)
        {
            entity.Location.Y = Height - 1;
        }
    }
}
