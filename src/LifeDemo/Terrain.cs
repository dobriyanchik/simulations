﻿using MersenneTwister;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace LifeDemo;

public enum TerrainType
{
    Normal,
    Low,
    High
}

public class Terrain
{
    private readonly int _randomSteps;
    private const bool ForceIntoBounds = true;

    private readonly Random random = Randoms.FastestInt32;

    public Terrain(int lengthX, int lengthY, TerrainType type, int maxHeight, int waterLevel)
    {
        int stepsMultiplier;

        switch (type)
        {
            case TerrainType.Low:
                stepsMultiplier = 3;
                break;
            case TerrainType.High:
                stepsMultiplier = 100;
                break;
            default:
                stepsMultiplier = 30;
                break;
        }

        MapLengthX = lengthX;
        MapLengthY = lengthY;
        MaxHeight = maxHeight;
        WaterLevel = waterLevel;
        _randomSteps = stepsMultiplier * MapLengthX * MapLengthY;

        Generate();
    }

    public Terrain(int lengthX, int lengthY) : this(lengthX, lengthY, TerrainType.Normal, 10, 0)
    {
    }

    public Terrain(int[,] heightMap)
    {
        MapLengthX = heightMap.GetLength(0);
        MapLengthY = heightMap.GetLength(1);
        HeightMap = heightMap;
    }

    public int MapLengthX { get; }

    public int MapLengthY { get; }

    public int WaterLevel { get; }

    public int MaxHeight { get; }

    public int[,] HeightMap { get; private set; }

    public void Generate()
    {
        HeightMap = new int[MapLengthX, MapLengthY];
        CreateRandomHeightMap();
        Normalize(MaxHeight);
        ShiftVertically(-WaterLevel);
    }

    public void Normalize(int newMax)
    {
        int max = HeightMap.Cast<int>().Max();
        float f = newMax / (float)max;

        for (int x = 0; x < MapLengthX; x++)
        {
            for (int y = 0; y < MapLengthY; y++)
            {
                HeightMap[x, y] = (int)(HeightMap[x, y] * f);
            }
        }
    }

    public void ShiftVertically(int offset)
    {
        for (int x = 0; x < MapLengthX; x++)
        {
            for (int y = 0; y < MapLengthY; y++)
            {
                HeightMap[x, y] += offset;
            }
        }
    }

    public Bitmap ToBitmap()
    {
        var bitmap = new Bitmap(MapLengthX, MapLengthY);

        for (int x = 0; x < MapLengthX; x++)
        {
            for (int y = 0; y < MapLengthY; y++)
            {
                int height = Math.Min(HeightMap[x, y], 255);
                var color = Color.FromArgb(255, height, height, height);
                bitmap.SetPixel(x, y, color);
            }
        }
        return bitmap;
    }

    public Bitmap ToColoredBitmap(List<System.Windows.Media.Color> colorMap) =>
        ToColoredBitmap(colorMap, new Size(MapLengthX, MapLengthY));

    public Bitmap ToColoredBitmap(List<System.Windows.Media.Color> colorMap, Size size)
    {
        var scaleX = (double)size.Width / MapLengthX;
        var scaleY = (double)size.Height / MapLengthY;

        int maxHeight = colorMap.Count;

        var map = HeightMap; //GetNormalizedMap(maxHeight);
        var bitmap = new Bitmap(size.Width, size.Height);

        for (int x = 0; x < MapLengthX; x++)
        {
            for (int y = 0; y < MapLengthY; y++)
            {
                int height = Math.Min(map[x, y], maxHeight);
                var col = colorMap[height + WaterLevel];

                for (int i = 0; i < scaleX; i++)
                {
                    for (int j = 0; j < scaleY; j++)
                    {
                        bitmap.SetPixel((int)(x * scaleX) + i, (int)(y * scaleY) + j, Color.FromArgb(col.R, col.G, col.B));
                    }
                }
            }
        }
        return bitmap;
    }

    public int[,] GetNormalizedMap(int newMax)
    {
        int max = HeightMap.Cast<int>().Max();
        float f = newMax / (float)max;

        int[,] newMap = new int[MapLengthX, MapLengthY];

        for (int x = 0; x < MapLengthX; x++)
        {
            for (int y = 0; y < MapLengthY; y++)
            {
                newMap[x, y] = (int)(HeightMap[x, y] * f);
            }
        }

        return newMap;
    }

    private void CreateRandomHeightMap()
    {
        var maxX = MapLengthX - 1;
        var maxY = MapLengthY - 1;

        int x = MapLengthX / 2;
        int y = MapLengthY / 2;

        for (int i = 0; i < _randomSteps; i++)
        {
            x += random.Next(-1, 2);
            y += random.Next(-1, 2);

            if (ForceIntoBounds)
            {
                x = Math.Max(x, 0);
                x = Math.Min(x, maxX);
                y = Math.Max(y, 0);
                y = Math.Min(y, maxY);
            }

            if (x >= 0 && x < MapLengthX && y >= 0 && y < MapLengthY)
            {
                HeightMap[x, y]++;
            }
        }
    }
}
