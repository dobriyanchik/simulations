﻿using System.Collections.Generic;
using System.Windows.Media;

namespace LifeDemo.Visualization;

internal enum TerrainPalette
{
    Grass,
    Sand
}

internal static class Palette
{
    public static List<Color> Current { get; set; } = grass;

    private static readonly List<Color> sand = new List<Color>
    {
        Color.FromArgb(255, 39, 41, 59),
        Color.FromArgb(255, 54, 56, 84),
        Color.FromArgb(255, 81, 84, 126),
        Color.FromArgb(255, 252, 220, 131),
        Color.FromArgb(255, 227, 151, 62),
        Color.FromArgb(255, 186, 134, 63),
        Color.FromArgb(255, 150, 91, 28),
        Color.FromArgb(255, 145, 112, 76),
        Color.FromArgb(255, 52, 36, 28),
        Color.FromArgb(255, 52, 44, 41)
    };

    private static readonly List<Color> grass = new List<Color>
    {
        Color.FromArgb(255, 39, 71, 90),
        Color.FromArgb(255, 55, 102, 130),
        Color.FromArgb(255, 69, 128, 163),
        Color.FromArgb(255, 140, 170, 140),
        Color.FromArgb(255, 77, 118, 83),
        Color.FromArgb(255, 31, 58, 53),
        Color.FromArgb(255, 99, 123, 98),
        Color.FromArgb(255, 213, 186, 162),
        Color.FromArgb(255, 187, 119, 96),
        Color.FromArgb(255, 250, 244, 241)
    };

    public static void SetPalette(TerrainPalette palette)
    {
        switch (palette)
        {
            case TerrainPalette.Grass:
                Current = grass;
                break;
            case TerrainPalette.Sand:
                Current = sand;
                break;
            default:
                Current = grass;
                break;
        }
    }
}
