﻿using SciSim.Core.Graphics;
using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;

namespace LifeDemo.Visualization;

public class ModelsFactory
{
    private readonly IModelCreator _cubeFactory;
    private readonly IModelCreator _sphereFactory;

    private readonly Model3DGroup _food;
    private readonly Model3DGroup _animal;
    private readonly Model3DGroup _animalSelected;
    private readonly Model3DGroup _predator;

    public ModelsFactory()
    {
        _cubeFactory = new CubesCreator();
        _sphereFactory = new SpheresCreator();

        _food = ConstructFood();
        _animal = ConstructAnimal(false);
        _animalSelected = ConstructAnimal(true);
        _predator = ConstructPredator();
    }

    public Model3DGroup Food => _food.Clone();

    public Model3DGroup Animal => _animal.Clone();

    public Model3DGroup SelectedAnimal => _animalSelected.Clone();

    public Model3DGroup Predator => _predator.Clone();

    private Model3DGroup ConstructFood()
    {
        var _foodMaterial = new DiffuseMaterial(Brushes.DarkViolet);

        var size = 6d;
        var halfSize = size / 2;

        Model3DGroup animalModel = new();

        var model = _sphereFactory.CreateModel(_foodMaterial);

        new Modificator()
            .Scale(size, size, size / 2)
            .Move(-halfSize, -halfSize, 0)
            .ApplyTo(model);

        animalModel.Children.Add(model);

        return animalModel;
    }

    private Model3DGroup ConstructAnimal(bool selected) =>
        MakeBacteria(12, selected);

    private Model3DGroup ConstructPredator() =>
        MakePredator(13);

    private DiffuseMaterial GetSkin(string skinUrl) =>
        new (new ImageBrush(new BitmapImage(new Uri(skinUrl, UriKind.Relative))));

    private Model3DGroup MakePredator(double size)
    {
        double halfSize = size / 2;
        DiffuseMaterial tailMaterial = new(Brushes.DarkRed);

        Model3DGroup blob = new();

        var tail1 = _cubeFactory.CreateModel(tailMaterial);
        var tail2 = _cubeFactory.CreateModel(tailMaterial);

        new Modificator()
            .Scale(1, halfSize * 0.75, 1)
            .Move(-2, -size * 0.7, size * 0.10)
            .ApplyTo(tail1);

        new Modificator()
            .Scale(1, halfSize * 0.75, 1)
            .Move(2, -size * 0.7, size * 0.10)
            .ApplyTo(tail2);

        var body = _sphereFactory.CreateModel(new DiffuseMaterial(Brushes.DarkOrange));
        var head = _sphereFactory.CreateModel(tailMaterial);

        new Modificator()
            .Scale(size, size, size * 0.25)
            .Move(-halfSize, -halfSize, 0)
            .ApplyTo(body);

        new Modificator()
            .Scale(size / 1.4, size / 1.4, size * 0.1)
            .Move(-size / 2.8, -size / 3.8, size * 0.15)
            .ApplyTo(head);

        blob.Children.Add(body);
        blob.Children.Add(head);
        blob.Children.Add(tail1);
        blob.Children.Add(tail2);

        blob.Freeze();

        return blob;
    }

    private Model3DGroup MakeBacteria(double size, bool selected)
    {
        double halfSize = size / 2;
        DiffuseMaterial tailMaterial = new(Brushes.DarkGreen);

        var material = new DiffuseMaterial(Brushes.Olive);
        var body1Material = new DiffuseMaterial(Brushes.Orange);
        var body2Material = new DiffuseMaterial(Brushes.Red);
        var eyeMaterial = new DiffuseMaterial(Brushes.DarkRed);

        Model3DGroup model = new();

        #region Body

        var body1 = _sphereFactory.CreateModel(material);
        var body2 = _sphereFactory.CreateModel(body1Material);
        var body3 = _sphereFactory.CreateModel(body2Material);

        new Modificator()
            .Scale(size * 0.7, size, size * 0.1)
            .Move(-halfSize * 0.7, -halfSize, 0)
            .ApplyTo(body1);

        new Modificator()
            .Scale(size * 0.55, size * 0.8, size * 0.1)
            .Move(-halfSize * 0.55, -halfSize * 0.75, size * 0.03)
            .ApplyTo(body2);

        new Modificator()
            .Scale(size * 0.45, size * 0.6, size * 0.1)
            .Move(-halfSize * 0.45, -halfSize * 0.45, size * 0.06)
            .ApplyTo(body3);

        if (selected)
        {
            SolidColorBrush selectionBrush = new(Colors.White)
            {
                Opacity = 0.25
            };

            EmissiveMaterial selectionMaterial = new(selectionBrush);
            var selection = _sphereFactory.CreateModel(selectionMaterial);

            new Modificator()
                .Scale(size * 1.5, size * 1.5, size * 0.05)
                .Move(-halfSize * 1.5, -halfSize * 1.5, 0)
                .ApplyTo(selection);

            model.Children.Add(selection);
        }

        model.Children.Add(body1);
        model.Children.Add(body2);
        model.Children.Add(body3);

        #endregion

        #region Tail

        var tail1 = _cubeFactory.CreateModel(tailMaterial);
        var tail2 = _cubeFactory.CreateModel(tailMaterial);
        var tail3 = _cubeFactory.CreateModel(tailMaterial);

        new Modificator()
            .Scale(0.5, halfSize, 0.5)
            .Move(-3, -halfSize, 0)
            .ApplyTo(tail1);

        new Modificator()
            .Scale(0.5, halfSize, 0.5)
            .Move(0, -size * 0.75, 0)
            .ApplyTo(tail2);

        new Modificator()
            .Scale(0.5, halfSize, 0.5)
            .Move(3, -halfSize, 0)
            .ApplyTo(tail3);

        model.Children.Add(tail1);
        model.Children.Add(tail2);
        model.Children.Add(tail3);

        #endregion

        #region Arms

        var arm1 = _cubeFactory.CreateModel(material);
        var arm2 = _cubeFactory.CreateModel(material);
        var arm3 = _cubeFactory.CreateModel(material);

        new Modificator()
            .Scale(size * 0.8, 0.5, 0.5)
            .Move(-halfSize * 0.8, halfSize * 0.3, 0)
            .ApplyTo(arm1);

        new Modificator()
            .Scale(size * 0.7, 0.5, 0.5)
            .Move(-halfSize * 0.7, halfSize * 0.4, 0)
            .ApplyTo(arm2);

        new Modificator()
            .Scale(size * 0.8, 0.5, 0.5)
            .Move(-halfSize * 0.8, halfSize * 0.5, 0)
            .ApplyTo(arm3);

        model.Children.Add(arm1);
        model.Children.Add(arm2);
        model.Children.Add(arm3);

        #endregion

        #region Eye

        var eye = _sphereFactory.CreateModel(eyeMaterial);

        new Modificator()
            .Scale(size * 0.2, size * 0.2, size * 0.1)
            .Move(-halfSize * 0.2, halfSize * 0.3, size * 0.09)
            .ApplyTo(eye);

        model.Children.Add(eye);

        #endregion

        model.Freeze();

        return model;
    }

    private Model3DGroup MakeBlob(double size, Material material)
    {
        var halfSize = size / 2;
        var eyeMaterial = new DiffuseMaterial(Brushes.White);

        var blob = new Model3DGroup();

        var eye1 = _cubeFactory.CreateModel(eyeMaterial);
        var eye2 = _cubeFactory.CreateModel(eyeMaterial);

        new Modificator()
            .Scale(1, halfSize * 0.75, 1)
            .Move(-2, 0, size * 0.75)
            .ApplyTo(eye1);

        new Modificator()
            .Scale(1, halfSize * 0.75, 1)
            .Move(2, 0, size * 0.75)
            .ApplyTo(eye2);

        var body = _sphereFactory.CreateModel(material);
        var head = _sphereFactory.CreateModel(material);

        new Modificator()
            .Scale(size, size, size * 0.8)
            .Move(-halfSize, -halfSize, 0)
            .ApplyTo(body);

        new Modificator()
            .Scale(size / 1.4, size / 1.4, size / 1.2)
            .Move(-size / 2.8, -size / 2.8, size * 0.25)
            .ApplyTo(head);

        blob.Children.Add(body);
        blob.Children.Add(head);
        blob.Children.Add(eye1);
        blob.Children.Add(eye2);

        blob.Freeze();

        return blob;
    }
}
