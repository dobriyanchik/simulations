﻿using LifeDemo.Simulation;
using SciSim.Core.Graphics;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;

namespace LifeDemo.Visualization;

public class EvolutionVisualizer
{
    private const double Rad = 180 / Math.PI;
    private const int HeightStep = 5;

    private readonly Vector3D _rotationVector = new(0, 0, 1);

    // Viewport and models.
    private readonly ModelsFactory _models;
    private readonly Viewport3D _viewport;
    private readonly ModelVisual3D _terrainVisual;
    private readonly ModelUIElement3D _ambientLight;
    private readonly ModelUIElement3D _directionalLight;
    private readonly Model3DGroup _entitiesModelGroup;

    private Model3DGroup _terrainModel;

    private int[,] map;
    private double scaleX, scaleY;

    public EvolutionVisualizer(Viewport3D viewport)
    {
        _viewport = viewport;

        _terrainVisual = new ModelVisual3D();
        _entitiesModelGroup = new Model3DGroup();

        _ambientLight = new ModelUIElement3D
        {
            Model = new AmbientLight(Color.FromArgb(255, 200, 200, 200))
        };

        _directionalLight = new ModelUIElement3D
        {
            Model = new DirectionalLight(Color.FromArgb(255, 200, 200, 200), new Vector3D(-1, -1, -2))
        };

        ModelVisual3D entitiesVisual = new()
        {
            Content = _entitiesModelGroup
        };

        _viewport.Children.Add(_ambientLight);
        _viewport.Children.Add(_directionalLight);
        _viewport.Children.Add(_terrainVisual);
        _viewport.Children.Add(entitiesVisual);

        _models = new ModelsFactory();
    }

    public void DrawEnvironment(EvolutionEnvironment environment, int selectedAgent)
    {
        _entitiesModelGroup.Children.Clear();

        int agentToSelect = 0;

        environment.FilterBy<Food>().ForEach(f => DrawFood(f));
        environment.FilterBy<Animal>().ForEach(a => DrawAnimal(a, selectedAgent == agentToSelect++));
        environment.FilterBy<Predator>().ForEach(p => DrawPredator(p));
    }

    public void SetTerrain(Terrain terrain, int landWidth, int landHeight)
    {
        _terrainModel = new Model3DGroup();

        scaleX = (double)landWidth / terrain.MapLengthX;
        scaleY = (double)landHeight / terrain.MapLengthY;

        //int[,] normalizedMap = terrain.GetNormalizedMap(Palette.Current.Count - 1);
        map = new int[landWidth, landHeight];

        for (int x = 0; x < landWidth; x++)
        {
            for (int y = 0; y < landHeight; y++)
            {
                map[x, y] = terrain.HeightMap[(int)(x / scaleX), (int)(y / scaleY)] * HeightStep;
            }
        }

        System.Drawing.Bitmap bitmap = terrain.ToColoredBitmap(Palette.Current, new System.Drawing.Size(landWidth, landHeight));

        ImageBrush terrainBrush = new()
        {
            ImageSource = Imaging.CreateBitmapSourceFromHBitmap(
                bitmap.GetHbitmap(),
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions())
        };


        double surfMapXCoeff = landWidth < 300 ? 1 : landWidth / 300;
        double surfMapYCoeff = landHeight < 300 ? 1 : landHeight / 300;
        int surfXSize = (int)(landWidth / surfMapXCoeff);
        int surfYSize = (int)(landHeight / surfMapYCoeff);
        int[,] mapToDraw = new int[surfXSize, surfYSize];

        for (int x = 0; x < surfXSize; x++)
        {
            for (int y = 0; y < surfYSize; y++)
            {
                mapToDraw[x, y] = map[(int)(x * surfMapXCoeff), (int)(y * surfMapYCoeff)];
            }
        }

        DiffuseMaterial materialGround = new(terrainBrush);

        GeometryModel3D surface = new SurfaceCreator().CreateSurface(mapToDraw, materialGround);

        new Modificator()
            .Scale(surfMapXCoeff, surfMapYCoeff, 1)
            .ApplyTo(surface);

        _terrainModel.Children.Add(surface);
        _terrainModel.Children.Add(GetWater(landWidth, landHeight));

        _terrainModel.Freeze();
        _terrainVisual.Content = _terrainModel;
    }

    private GeometryModel3D GetWater(int landWidth, int landHeight)
    {
        Point3DCollection waterPoints = new();
        Int32Collection triangleIndices = new();

        int triangleCounter;
        int waterLevel = -1;

        for (int i = 0; i < 1; i++)
        {
            triangleCounter = waterPoints.Count;
            double z = waterLevel - i * HeightStep;

            waterPoints.Add(new Point3D(0, 0, z));
            waterPoints.Add(new Point3D(landWidth, landHeight, z));
            waterPoints.Add(new Point3D(0, landHeight, z));
            waterPoints.Add(new Point3D(landWidth, 0, z));

            triangleIndices.Add(triangleCounter);
            triangleIndices.Add(triangleCounter + 1);
            triangleIndices.Add(triangleCounter + 2);
            triangleIndices.Add(triangleCounter);
            triangleIndices.Add(triangleCounter + 3);
            triangleIndices.Add(triangleCounter + 1);
        }

        SolidColorBrush waterBrush = new(Colors.Blue)
        { 
            Opacity = 0.2 
        };

        MeshGeometry3D meshGeometry = new()
        {
            Positions = waterPoints,
            TriangleIndices = triangleIndices
        };

        GeometryModel3D myWaterGeometryModel = new(meshGeometry, new EmissiveMaterial(waterBrush));

        return myWaterGeometryModel;
    }

    private void DrawFood(Food food)
    {
        double x = food.Location.X;
        double y = food.Location.Y;
        int height = GetHeight(x, y);
        Model3DGroup foodModel = _models.Food;

        new Modificator()
            .Move(x, y, height)
            .ApplyTo(foodModel);

        _entitiesModelGroup.Children.Add(foodModel);
    }

    private void DrawAnimal(Animal animal, bool selected)
    {
        double x = animal.Location.X;
        double y = animal.Location.Y;
        int height = GetHeight(x, y);
        double angle = animal.Angle * Rad;

        Model3DGroup animalModel = selected ? _models.SelectedAnimal : _models.Animal;

        new Modificator()
            .Move(x, y, height)
            .Rotate(_rotationVector, angle, new Point3D(x, y, 0))
            .ApplyTo(animalModel);

        _entitiesModelGroup.Children.Add(animalModel);
    }

    private void DrawPredator(Predator predator)
    {
        double x = predator.Location.X;
        double y = predator.Location.Y;
        int height = GetHeight(x, y);
        double angle = predator.Angle * Rad;

        Model3DGroup predatorModel = _models.Predator;

        new Modificator()
            .Move(x, y, height)
            .Rotate(_rotationVector, angle, new Point3D(x, y, 0))
            .ApplyTo(predatorModel);

        _entitiesModelGroup.Children.Add(predatorModel);
    }

    private int GetHeight(double x, double y) =>
        map[(int)x, (int)y];
}
