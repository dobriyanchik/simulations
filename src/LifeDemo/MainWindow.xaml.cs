﻿using LifeDemo.Evolution;
using LifeDemo.NeuralNet;
using LifeDemo.Simulation;
using LifeDemo.Visualization;
using MersenneTwister;
using OxyPlot;
using OxyPlot.Series;
using SciML.GeneticAlgorithm;
using SciML.NeuralNetwork.Evolution;
using SciSim.NNVisualization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Threading;

namespace LifeDemo;

public partial class MainWindow : Window
{
    private readonly Dictionary<int, int> _simulationStepDelays = new Dictionary<int, int>
    {
        { 0, 40 },
        { 1, 1 },
        { 2, 0 },
    };

    private BulkGeneticEngine<AnimalBrain, double> geneticEngine;
    private BulkGeneticEngine<PredatorBrain, double> predatorsGeneticEngine;

    private EvolutionEnvironment environment;
    private EvolvingNeuralNetVisualizer brainVisualizator;
    private EvolutionVisualizer visualizator;
    private readonly Random _random = Randoms.FastestDouble;
    private int populationNumber = 0;
    private volatile bool play = false;
    private Terrain terrain;

    private CancellationTokenSource tokenSource;

    private int simulationStepDelay;

    private Point mouseMoveStart;

    public MainWindow()
    {
        simulationStepDelay = _simulationStepDelays[1];
        InitializeComponent();

        foreach (string type in typeof(TerrainType).GetEnumNames())
        {
            comboTerrainType.Items.Add(type);
        }

        comboTerrainType.SelectedIndex = 0;

        foreach (string type in typeof(TerrainPalette).GetEnumNames())
        {
            comboPalette.Items.Add(type);
        }

        comboPalette.SelectedIndex = 0;

        fitnessPlot.Model.Title = "Fitness";
        fitnessPlot.Model.Axes[0].IsAxisVisible = false;
        fitnessPlot.Model.Axes[1].IsAxisVisible = false;
    }

    private void EnvironmentStep()
    {
        while (true)
        {
            if (play)
            {
                Task.Delay(simulationStepDelay).Wait();
                environment.Iterate();
            }
        }
    }

    private void DrawSimulation()
    {
        while (true)
        {
            if (play)
            {
                int selectedAgentIndex = 0;

                comboActiveAnimal.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => 
                    selectedAgentIndex = (int)comboActiveAnimal.SelectedItem));

                viewport.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => 
                    visualizator.DrawEnvironment(environment, selectedAgentIndex)));

                viewportBrain.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => 
                    brainVisualizator.DrawBrain(environment.FilterBy<Animal>().ElementAt(selectedAgentIndex).Brain)));
            }
        }
    }

    private void InitializeEnvironment(int envWidth, int envHeight, int agentsCount, int foodCount, int predatorsCount)
    {
        environment = new EvolutionEnvironment(envWidth, envHeight);
        environment.OnUpdate += new EatenFoodObserver().Notify;
        environment.OnUpdate += new EatenAnimalObserver().Notify;
        environment.InitializePredators(predatorsGeneticEngine.BestChromosome, predatorsCount);
        environment.InitializeAnimals(geneticEngine.BestChromosome, agentsCount);
        environment.InitializeFood(foodCount);
    }

    private void InitializeGeneticAlgorithm(int populationSize, int parentalChromosomesSurviveCount, AnimalBrain baseNeuralNetwork)
    {
        Population<AnimalBrain> brains = new ();

        for (int i = 0; i < (populationSize - 1); i++)
        {
            AnimalBrain brain = baseNeuralNetwork == null ? 
                AnimalBrain.GetRandomBrain() : 
                baseNeuralNetwork.Mutate();

            brains.AddChromosome(brain);
        }

        AnimalBrain baseBrain = baseNeuralNetwork ?? AnimalBrain.GetRandomBrain();
        brains.AddChromosome(baseBrain);

        AnimalFitness fit = new();

        geneticEngine = new BulkGeneticEngine<AnimalBrain, double>(brains, fit)
        { 
            ParentChromosomesSurviveCount = parentalChromosomesSurviveCount 
        };

        geneticEngine.OnIterate += UpdateAnimalsEvolutionProgress;
        geneticEngine.OnIterate += GetIterationUpdate;
    }

    private void InitializePredatorsGeneticAlgorithm(int populationSize, int parentalChromosomesSurviveCount, PredatorBrain baseNeuralNetwork)
    {
        Population<PredatorBrain> brains = new();

        for (int i = 0; i < (populationSize - 1); i++)
        {
            PredatorBrain brain = baseNeuralNetwork == null ? 
                PredatorBrain.GetRandomBrain() : 
                baseNeuralNetwork.Mutate();

            brains.AddChromosome(brain);
        }

        PredatorBrain baseBrain = baseNeuralNetwork ?? PredatorBrain.GetRandomBrain();
        brains.AddChromosome(baseBrain);

        PredatorFitness fit = new();

        predatorsGeneticEngine = new BulkGeneticEngine<PredatorBrain, double>(brains, fit)
        {
            ParentChromosomesSurviveCount = parentalChromosomesSurviveCount
        };

        predatorsGeneticEngine.OnIterate += UpdatePredatorsEvolutionProgress;
        predatorsGeneticEngine.OnIterate += GetPredatorIterationUpdate;
    }

    private void SetControlsState(bool isEnabled)
    {
        buttonEvolve.IsEnabled = isEnabled;
        buttonPause.IsEnabled = isEnabled;
        textboxEvolve.IsEnabled = isEnabled;
        checkboxMovingFood.IsEnabled = isEnabled;
        checkboxRegenerate.IsEnabled = isEnabled;
    }

    private void buttonPause_Click(object sender, RoutedEventArgs e)
    {
        play = !play;
        buttonPause.Content = play ? "Pause" : "Play";
    }

    private void checkboxRegenerate_Checked(object sender, RoutedEventArgs e) =>
        environment.RegenerateFood = true;

    private void checkboxRegenerate_Unchecked(object sender, RoutedEventArgs e) =>
        environment.RegenerateFood = false;

    public void GetIterationUpdate(BulkGeneticEngine<AnimalBrain, double> ga)
    {
        double fit = ga.GetFitnessOf(ga.BestChromosome);

        Dispatcher.CurrentDispatcher.Invoke(() =>
        {
            (fitnessPlot.Model.Series[0] as LineSeries).Points.Add(new DataPoint(ga.Iteration, fit));
            fitnessPlot.Model.Axes[1].MajorStep = (int)fitnessPlot.Model.Axes[1].ActualMaximum;
            fitnessPlot.InvalidatePlot(true);
        });
    }

    public void GetPredatorIterationUpdate(BulkGeneticEngine<PredatorBrain, double> ga)
    {
        double fit = ga.GetFitnessOf(ga.BestChromosome);

        Dispatcher.CurrentDispatcher.Invoke(() =>
        {
            (fitnessPlot.Model.Series[1] as LineSeries).Points.Add(new DataPoint(ga.Iteration, fit));
            fitnessPlot.Model.Axes[1].MajorStep = (int)fitnessPlot.Model.Axes[1].ActualMaximum;
            fitnessPlot.InvalidatePlot(true);
        });
    }

    public void UpdateAnimalsEvolutionProgress(BulkGeneticEngine<AnimalBrain, double> ga)
    {
        int iterCount = GetIntFrom(textboxEvolve);
        int iteration = ga.Iteration % iterCount;

        if (iteration == 0)
        {
            iteration = iterCount;
        }

        double value = iteration * 50d / iterCount;
        progressBar.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => progressBar.Value = value));
    }

    public void UpdatePredatorsEvolutionProgress(BulkGeneticEngine<PredatorBrain, double> ga)
    {
        int iterCount = GetIntFrom(textboxEvolve);
        int iteration = ga.Iteration % iterCount;

        if (iteration == 0)
        {
            iteration = iterCount;
        }

        double value = iteration * 50d / iterCount + 50;
        progressBar.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => progressBar.Value = value));
    }

    private void viewport_MouseWheel(object sender, MouseWheelEventArgs e) =>
        Camera.Width -= (double)e.Delta / 10;

    private void MoveCamera(object sender, KeyEventArgs e)
    {
        // Scrolling by moving the 3D camera
        double x = 0;
        double y = 0;

        if (e.Key == Key.Right)
        {
            x = +10;
            y = -10;
        }

        if (e.Key == Key.Down)
        {
            x = -10;
            y = -10;
        }

        if (e.Key == Key.Left)
        {
            x = -10;
            y = +10;
        }

        if (e.Key == Key.Up)
        {
            x = +10;
            y = +10;
        }

        Point3D cameraPosition = new(Camera.Position.X + x, Camera.Position.Y + y, Camera.Position.Z);
        Camera.Position = cameraPosition;
    }

    private void buttonGenerate_Click(object sender, RoutedEventArgs e) =>
        GenerateTerrain();

    private void Window_ContentRendered(object sender, EventArgs e) =>
        visualizator = new EvolutionVisualizer(viewport);

    private void GenerateTerrain()
    {
        Palette.SetPalette(EnumParser.Parse<TerrainPalette>(comboPalette.Text));
        var terrainType = EnumParser.Parse<TerrainType>(comboTerrainType.Text);

        int lengthX = GetIntFrom(textboxLandSamplesWidth);
        int lengthY = GetIntFrom(textboxLandSamplesHeight);
        terrain = new Terrain(lengthX, lengthY, terrainType, 9, 3);

        System.Drawing.Size size = new((int)topViewImage.Width, (int)topViewImage.Height);
        SetTopImage(terrain.ToColoredBitmap(Palette.Current, size));

        visualizator.SetTerrain(terrain, GetIntFrom(textboxLandWidth), GetIntFrom(textboxLandHeight));

        if (environment != null)
        {
            environment.Width = GetIntFrom(textboxLandWidth);
            environment.Height = GetIntFrom(textboxLandHeight);
        }
    }

    private void SetTopImage(System.Drawing.Bitmap bitmap)
    {
        using MemoryStream memory = new();
        bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
        memory.Position = 0;
        BitmapImage bitmapimage = new();
        bitmapimage.BeginInit();
        bitmapimage.StreamSource = memory;
        bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
        bitmapimage.EndInit();

        topViewImage.Source = bitmapimage;
    }

    private void buttonStartNew_Click(object sender, RoutedEventArgs e)
    {
        int animalsCount = GetIntFrom(textboxLAnimals);
        int predatorsCount = GetIntFrom(textboxPredators);
        int foodCount = GetIntFrom(textboxFood);

        int populationSize = GetIntFrom(textboxPopulationSize);
        int parentalChromosomesSurviveCount = GetIntFrom(textboxSurvive);

        InitializeGeneticAlgorithm(populationSize, parentalChromosomesSurviveCount, null);
        InitializePredatorsGeneticAlgorithm(populationSize, parentalChromosomesSurviveCount, null);
        InitializeEnvironment(GetIntFrom(textboxLandWidth), GetIntFrom(textboxLandHeight), animalsCount, foodCount, predatorsCount);

        if (terrain == null)
        {
            GenerateTerrain();
        }

        brainVisualizator = new EvolvingNeuralNetVisualizer(viewportBrain);

        checkboxMovingFood.IsChecked = false;
        checkboxRegenerate.IsChecked = true;

        for (int i = 0; i < animalsCount; i++)
        {
            comboActiveAnimal.Items.Add(i);
        }

        comboActiveAnimal.SelectedItem = 0;

        play = true;

        tokenSource?.Cancel();
        tokenSource?.Dispose();
        tokenSource = new CancellationTokenSource();

        new Task(() => EnvironmentStep(), tokenSource.Token).Start();
        new Task(() => DrawSimulation(), tokenSource.Token).Start();
    }

    private void ZoomViewport(object sender, MouseEventArgs e)
    {
        if (e.LeftButton == MouseButtonState.Pressed)
        {
            Point newPoint = e.GetPosition(viewport);

            // Scrolling by moving the 3D camera
            double dx = -(newPoint.X - mouseMoveStart.X) / 4;
            double dy = (newPoint.Y - mouseMoveStart.Y) / 4;

            double x = dx + dy;
            double y = -dx + dy;

            Point3D cameraPosition = new Point3D(
                Camera.Position.X + x,
                Camera.Position.Y + y,
                Camera.Position.Z);
            Camera.Position = cameraPosition;

            mouseMoveStart = newPoint;
        }
    }

    private void viewport_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e) =>
        mouseMoveStart = e.GetPosition(viewport);

    private void checkboxMovingFood_Checked(object sender, RoutedEventArgs e) =>
        environment.ChangeFoodType(false);

    private void checkboxMovingFood_Unchecked(object sender, RoutedEventArgs e) =>
        environment.ChangeFoodType(true);

    private int GetIntFrom(TextBox textbox) => Convert.ToInt32(textbox.Text);

    private void buttonEvolve_Click_1(object sender, RoutedEventArgs e)
    {
        buttonEvolve.Visibility = Visibility.Hidden;

        play = false;
        progressBar.Value = 0;
        SetControlsState(false);

        int iterCount = Convert.ToInt32(textboxEvolve.Text);

        geneticEngine.Evolve(iterCount);
        EvolvingNetBase newBrain = geneticEngine.BestChromosome;
        environment.SetAnimalsBrains(newBrain);

        //Predators
        predatorsGeneticEngine.Evolve(iterCount);
        EvolvingNetBase newPredatorBrain = predatorsGeneticEngine.BestChromosome;
        environment.SetPredatorsBrains(newPredatorBrain);

        populationNumber += iterCount;
        Title = $"Population: {populationNumber}";
        SetControlsState(true);
        play = true;
        progressBar.Value = 0;

        buttonEvolve.Visibility = Visibility.Visible;
    }

    private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) =>
        simulationStepDelay = _simulationStepDelays[(int)(sender as Slider).Value];

    private void PlaceEntity(object sender, MouseButtonEventArgs e)
    {
        Point p = e.GetPosition(topViewImage);

        double xRatio = environment.Width / topViewImage.Width;
        double yRatio = environment.Height / topViewImage.Height;

        Point translatePoint = new(p.X * xRatio, p.Y * yRatio);

        if (e.ChangedButton == MouseButton.Left)
        {
            Food food = environment.CreateRandomFood(1, 1);
            food.Location.X = translatePoint.X;
            food.Location.Y = translatePoint.Y;
            environment.AddEntity(food);
        } 
        else if (e.ChangedButton == MouseButton.Right)
        {
            double angle = 2 * Math.PI * _random.NextDouble();
            Animal animal = new(translatePoint.X, translatePoint.Y, angle);
            AnimalBrain brain = geneticEngine.BestChromosome.Clone() as AnimalBrain;
            animal.SetBrain(brain);
            environment.AddEntity(animal);
        }
    }
}
