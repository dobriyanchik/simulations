﻿using OxyPlot;
using OxyPlot.Series;

namespace LifeDemo;

public class MainViewModel
{
    public MainViewModel()
    {
        MyModel = new PlotModel();// { Title = "Fitness" };
        MyModel.Series.Add(new LineSeries());
        MyModel.Series.Add(new LineSeries() { Color = OxyColor.FromRgb(205, 92, 92)});
        MyModel.TextColor = OxyColor.FromRgb(255, 255, 255);
        MyModel.PlotAreaBorderThickness = new OxyThickness(0);
    }

    public PlotModel MyModel { get; }
}
