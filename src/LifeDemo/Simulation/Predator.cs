﻿using SciSim.Core.Math;
using System;
using System.Collections.Generic;

namespace LifeDemo.Simulation;

public class Predator : SmartEntity
{
    private const double MaxPredatorSpeed = 2;
    protected const double MaxEntitiesDistance = 10;

    //private const double PredatorExcitation = -10;
    private const double EmptyExcitation = 0;
    private const double AnimalExcitation = 10;

    public Predator(double x, double y, double angle) : base(x, y, angle) 
    { 
    }

    protected override double MaxSpeed => MaxPredatorSpeed;

    protected override List<double> CalculateNeuralInputs(EvolutionEnvironment environment)
    {
        // Find nearest animal
        Animal nearestAnimal = null;
        double nearestAnimalDist = double.MaxValue;

        foreach (Animal currFood in environment.FilterBy<Animal>())
        {
            // agent can see only ahead
            if (environment.IsInSight(this, currFood))
            {
                double currFoodDist = environment.DistanceBetween(this, currFood);

                if ((nearestAnimal == null) || (currFoodDist <= nearestAnimalDist))
                {
                    nearestAnimal = currFood;
                    nearestAnimalDist = currFoodDist;
                }
            }
        }

        // Find nearest agent
        SmartEntity nearestEntity = null;
        double nearestEntityDistantion = MaxEntitiesDistance;

        ////foreach (Entity currAgent in environment.FilterBy<Entity>())
        ////{
        ////    // agent can see only ahead
        ////    if ((this != currAgent) && (this.InSight(currAgent)))
        ////    {
        ////        double currAgentDist = this.DistanceTo(currAgent);

        ////        if (currAgentDist <= nearestEntityDistantion)
        ////        {
        ////            nearestEntity = currAgent;
        ////            nearestEntityDistantion = currAgentDist;
        ////        }
        ////    }
        ////}

        var neuralInputs = new List<double>();

        if (nearestAnimal != null)
        {
            Vector2D foodDirectionVector = nearestAnimal.Location - Location;

            // left/right cos
            double foodDirectionCosTeta =
                Math.Sign(Algebra.Determinant(MoveDirection, foodDirectionVector)) * 
                Algebra.CosTheta(MoveDirection, foodDirectionVector);

            neuralInputs.Add(AnimalExcitation);
            neuralInputs.Add(nearestAnimalDist);
            neuralInputs.Add(foodDirectionCosTeta);

        }
        else
        {
            neuralInputs.Add(EmptyExcitation);
            neuralInputs.Add(EmptyExcitation);
            neuralInputs.Add(EmptyExcitation);
        }

        if (nearestEntity != null)
        {
            Vector2D agentDirectionVector = nearestEntity.Location - Location;

            // left/right cos
            double agentDirectionCosTeta =
                Math.Sign(Algebra.Determinant(MoveDirection, agentDirectionVector)) * 
                Algebra.CosTheta(MoveDirection, agentDirectionVector);

            //neuralInputs.Add(EntityExcitation);
            neuralInputs.Add(nearestEntityDistantion);
            neuralInputs.Add(agentDirectionCosTeta);
        }
        else
        {
            neuralInputs.Add(EmptyExcitation);
            neuralInputs.Add(EmptyExcitation);
            neuralInputs.Add(EmptyExcitation);
        }

        return neuralInputs;
    }
}
