﻿using SciML.NeuralNetwork.Evolution;
using SciSim.Core.Math;
using SciSim.Core.Simulation;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace LifeDemo.Simulation;

public abstract class SmartEntity : IMovingEntity
{
    protected const double MaxDeltaAngle = 0.25;

    protected volatile EvolvingNetBase brain;

    public SmartEntity(double x, double y, double angle)
    {
        Location = new Vector2D(x, y);
        MoveDirection = new Vector2D();
        Speed = 0;
        Angle = angle;
        CalculateR();
    }

    public EvolvingNetBase Brain => brain;

    public Vector2D Location { get; set; }

    public double Angle { get; set; }

    public double Speed { get; set; }

    public Vector2D MoveDirection { get; }

    protected abstract double MaxSpeed { get; }

    public void Move() =>
        Location += MoveDirection * Speed;

    /// <summary>
    /// Animating of agents and evolving best brain - might be in different threads<br/>
    /// Synchronization prevents from race condition when trying to set new brain, while method "interact" runs<br/><br/>
    /// TODO Maybe consider to use non-blocking technique.But at the moment this simplest solution doesn't cause any overheads
    /// </summary>
    /// <param name="brain"></param>
    [MethodImpl(MethodImplOptions.Synchronized)]
    public void SetBrain(EvolvingNetBase brain)
    {
        this.brain = brain;
    }

    /// <summary>
    /// Synchronization prevents from race condition when trying to set new brain, while method "interact" runs<br/><br/>
    /// TODO Maybe consider to use non-blocking technique. But at the moment this simplest solution doesn't cause any overheads
    /// </summary>
    /// <param name="env"></param>
    [MethodImpl(MethodImplOptions.Synchronized)]
    public void InteractWith<T>(T environment) where T : Environment<T>
    {
        List<double> nnInputs = CalculateNeuralInputs(environment as EvolutionEnvironment);

        ActivateBrain(nnInputs);

        double deltaAngle = brain.OutputLayer.Neurons[0].Outputs[0].Signal;
        double deltaSpeed = brain.OutputLayer.Neurons[1].Outputs[0].Signal;

        deltaSpeed = Adjustment.AvoidNaNAndInfinity(deltaSpeed);
        deltaAngle = Adjustment.AvoidNaNAndInfinity(deltaAngle);

        double newSpeed = Adjustment.NormalizeValue(Speed + deltaSpeed, MaxSpeed);
        double newAngle = Angle + Adjustment.NormalizeValue(deltaAngle, MaxDeltaAngle);

        Angle = newAngle;
        CalculateR();
        Speed = newSpeed;

        Move();
    }

    protected void ActivateBrain(List<double> nnInputs)
    {
        for (int i = 0; i < nnInputs.Count; i++)
        {
            brain.InputLayer.Neurons[i].Inputs[0].Signal = nnInputs[i];
        }

        brain.Process();
    }

    protected abstract List<double> CalculateNeuralInputs(EvolutionEnvironment environment);

    protected void CalculateR()
    {
        MoveDirection.X = -Math.Sin(Angle);
        MoveDirection.Y = Math.Cos(Angle);
    }
}
