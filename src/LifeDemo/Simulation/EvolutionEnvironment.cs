﻿using MersenneTwister;
using SciML.NeuralNetwork.Evolution;
using SciSim.Core.Simulation;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace LifeDemo.Simulation;

public class EvolutionEnvironment : Environment<EvolutionEnvironment>
{
    private readonly Random _random;
    private volatile bool staticFood = true;

    public EvolutionEnvironment(int width, int height) : base(width, height)
    {
        _random = Randoms.WellBalanced;
    }

    public bool RegenerateFood { get; set; } = true;

    [MethodImpl(MethodImplOptions.Synchronized)]
    protected override void TimeStep()
    {
        foreach (IEntity entity in Entities)
        {
            entity.InteractWith(this);
            AvoidMovingOutsideOfBounds(entity);
        }
    }

    public void InitializeFood(int foodCount)
    {
        for (int i = 0; i < foodCount; i++)
        {
            Food food = CreateRandomFood(Width, Height);
            AddEntity(food);
        }
    }

    public void InitializeAnimals(EvolvingNetBase brain, int agentsCount)
    {
        for (int i = 0; i < agentsCount; i++)
        {
            var animal = CreateRandomAnimal(brain, Width, Height);
            AddEntity(animal);
        }
    }

    public void SetAnimalsBrains(EvolvingNetBase newBrain)
    {
        foreach (Animal agent in FilterBy<Animal>())
        {
            agent.SetBrain(newBrain.Clone() as EvolvingNetBase);
        }
    }

    public void InitializePredators(EvolvingNetBase brain, int agentsCount)
    {
        for (int i = 0; i < agentsCount; i++)
        {
            AddEntity(CreateRandomPredator(brain, Width, Height));
        }
    }

    public void SetPredatorsBrains(EvolvingNetBase newBrain)
    {
        foreach (Predator agent in FilterBy<Predator>())
        {
            agent.SetBrain(newBrain.Clone() as EvolvingNetBase);
        }
    }

    public Food CreateRandomFood(int width, int height)
    {
        int x = _random.Next(width);
        int y = _random.Next(height);

        if (staticFood)
        {
            return new Food(x, y);
        }
        else
        {
            double speed = _random.NextDouble();
            double direction = _random.NextDouble() * 2 * Math.PI;

            return new MovingFood(x, y, direction, speed);
        }
    }

    public Animal CreateRandomAnimal(EvolvingNetBase brain, int width, int height)
    {
        int x = _random.Next(width);
        int y = _random.Next(height);
        double direction = _random.NextDouble() * 2 * Math.PI;

        Animal animal = new(x, y, direction);
        animal.SetBrain(brain);
        return animal;
    }

    public Predator CreateRandomPredator(EvolvingNetBase brain, int width, int height)
    {
        int x = _random.Next(width);
        int y = _random.Next(height);
        double direction = _random.NextDouble() * 2 * Math.PI;

        Predator animal = new(x, y, direction);
        animal.SetBrain(brain);
        return animal;
    }

    public void ChangeFoodType(bool staticFood)
    {
        this.staticFood = staticFood;
        var food = new List<Food>(FilterBy<Food>());

        foreach (Food f in food)
        {
            RemoveEntity(f);

            Food newFood = CreateRandomFood(1, 1);
            newFood.Location.X = f.Location.X;
            newFood.Location.Y = f.Location.Y;

            AddEntity(newFood);
        }
    }
}
