﻿using SciSim.Core.Math;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LifeDemo.Simulation;

public class Animal : SmartEntity
{
    private const double MaxAnimalSpeed = 2;
    protected const double MaxAnimalsDistance = 10;
    protected const double MaxDistanceToPredator = 200;

    ////private const double PredatorExcitation = -20;
    private const double AnimalExcitation = -10;
    private const double ZeroExcitation = 0;
    private const double FoodExcitation = 10;

    public Animal(double x, double y, double angle) : base(x, y, angle) 
    { 
    }

    protected override double MaxSpeed => MaxAnimalSpeed;

    protected override List<double> CalculateNeuralInputs(EvolutionEnvironment environment)
    {
        Food nearestFood = null;
        double nearestFoodDist = double.MaxValue;

        Animal nearestAnimal = null;
        double nearestAnimalDistantion = MaxAnimalsDistance;

        //Predator nearestPredator = null;
        //double nearestPredatorDistantion = MaxDistanceToPredator;

        // Find nearest food, animal can see only ahead
        var foodInSight = environment.FilterBy<Food>().Where(f => environment.IsInSight(this, f));

        foreach (var currFood in foodInSight)
        {
            double currFoodDist = environment.DistanceBetween(this, currFood);

            if (currFoodDist <= nearestFoodDist)
            {
                nearestFood = currFood;
                nearestFoodDist = currFoodDist;
            }
        }

        // Find nearest animal, animal can see only ahead
        var animalsInSight = environment.FilterBy<Animal>().Where(a => !Equals(a) && environment.IsInSight(this, a));

        foreach (var animal in animalsInSight)
        {
            double animalDist = environment.DistanceBetween(this, animal);

            if (animalDist <= nearestAnimalDistantion)
            {
                nearestAnimal = animal;
                nearestAnimalDistantion = animalDist;
            }
        }

        //// Find nearest predator, when animal runs from predator it does not need to see him
        //var predatorsInSight = environment.FilterBy<Predator>();//.Where(p => this.InSight(p));

        //foreach (var predator in predatorsInSight)
        //{
        //    double predatorDist = this.DistanceTo(predator);

        //    if (predatorDist <= nearestPredatorDistantion)
        //    {
        //        nearestPredator = predator;
        //        nearestPredatorDistantion = predatorDist;
        //    }
        //}

        var neuralInputs = new List<double>();

        if (nearestFood != null)
        {
            Vector2D direction = nearestFood.Location - Location;

            // left/right cos
            double directionCosTeta =
                Math.Sign(Algebra.Determinant(MoveDirection, direction)) * 
                Algebra.CosTheta(MoveDirection, direction);

            neuralInputs.Add(FoodExcitation);
            neuralInputs.Add(nearestFoodDist);
            neuralInputs.Add(directionCosTeta);
        }
        else
        {
            neuralInputs.Add(ZeroExcitation);
            neuralInputs.Add(ZeroExcitation);
            neuralInputs.Add(ZeroExcitation);
        }

        if (nearestAnimal != null)
        {
            Vector2D direction = nearestAnimal.Location - Location;

            // left/right cos
            double directionCosTeta =
                Math.Sign(Algebra.Determinant(MoveDirection, direction)) *
                Algebra.CosTheta(MoveDirection, direction);

            neuralInputs.Add(AnimalExcitation);
            neuralInputs.Add(nearestAnimalDistantion);
            neuralInputs.Add(directionCosTeta);
        }
        else
        {
            neuralInputs.Add(ZeroExcitation);
            neuralInputs.Add(ZeroExcitation);
            neuralInputs.Add(ZeroExcitation);
        }

        //if (nearestPredator != null)
        //{
        //    double directionVectorX = nearestPredator.X - this.X;
        //    double directionVectorY = nearestPredator.Y - this.Y;

        //    // left/right cos
        //    double directionCosTeta =
        //            Math.Sign(this.PseudoScalarProduct(this.Rx, this.Ry, directionVectorX, directionVectorY))
        //                    * this.CosTeta(this.Rx, this.Ry, directionVectorX, directionVectorY);

        //    neuralInputs.Add(PredatorExcitation);
        //    neuralInputs.Add(nearestPredatorDistantion);
        //    neuralInputs.Add(directionCosTeta);
        //}
        //else
        //{
        //    neuralInputs.Add(ZeroExcitation);
        //    neuralInputs.Add(ZeroExcitation);
        //    neuralInputs.Add(ZeroExcitation);
        //}

        return neuralInputs;
    }
}
