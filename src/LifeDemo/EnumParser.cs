﻿using System;

namespace LifeDemo;

internal static class EnumParser
{
    internal static T Parse<T>(string value) =>
        (T)Enum.Parse(typeof(T), value);
}
