﻿using MersenneTwister;
using SciML.GeneticAlgorithm;
using SciML.NeuralNetwork.Entities;
using SciML.NeuralNetwork.Evolution;
using SciML.NeuralNetwork.Evolution.Activation;
using System;
using System.Collections.Generic;

namespace LifeDemo.NeuralNet;

public class AnimalBrain : EvolvingNetBase, IChromosome<AnimalBrain>, ICloneable
{
    private const int InputsCount = 6;
    private const int HiddenCount = 7;
    private const int OutputsCount = 2;

    //private const int InputsCount = 9;
    //private const int HiddenCount = 10;
    //private const int OutputsCount = 2;

    private readonly Random _randomizer = Randoms.FastestDouble;

    public AnimalBrain() : base(InputsCount, HiddenCount, OutputsCount) 
    { 
    }

    public static AnimalBrain GetRandomBrain()
    {
        AnimalBrain randomBrain = new();
        randomBrain.ConstructNetwork();
        return randomBrain;
    }

    public List<AnimalBrain> Crossover(AnimalBrain anotherChromosome) =>
        base.Crossover(anotherChromosome);

    public AnimalBrain Mutate() =>
        base.Mutate<AnimalBrain>();

    public override void ConstructNetwork()
    {
        for (int i = 0; i < InputsCount; i++)
        {
            LinearFunction f = new();
            EvolvingNeuron n = new(f);
            n.Inputs.Add(new Synapse(i, i, 1));
            InputLayer.Neurons[i] = n;
        }

        for (int i = 0; i < HiddenCount; i++)
        {
            EvolvingActivationFunctionBase f = EvolvingActivationFunctionBase.GetRandomFunction();
            EvolvingNeuron n = new(f);
            HiddenLayer.Neurons[i] = n;
        }

        for (int i = 0; i < OutputsCount; i++)
        {
            EvolvingActivationFunctionBase f = EvolvingActivationFunctionBase.GetRandomFunction();
            EvolvingNeuron n = new(f);
            n.Outputs.Add(new Synapse(i, i, 1));
            OutputLayer.Neurons[i] = n;
        }

        Connections[0].Add(new Synapse(0, 0, _randomizer.NextDouble()));
        Connections[0].Add(new Synapse(0, 1, _randomizer.NextDouble()));
        Connections[0].Add(new Synapse(0, 2, _randomizer.NextDouble()));

        Connections[0].Add(new Synapse(1, 0, _randomizer.NextDouble()));
        Connections[0].Add(new Synapse(1, 1, _randomizer.NextDouble()));
        Connections[0].Add(new Synapse(1, 2, _randomizer.NextDouble()));
        Connections[0].Add(new Synapse(1, 3, _randomizer.NextDouble()));

        Connections[0].Add(new Synapse(2, 2, _randomizer.NextDouble()));
        Connections[0].Add(new Synapse(2, 3, _randomizer.NextDouble()));
        Connections[0].Add(new Synapse(2, 4, _randomizer.NextDouble()));

        Connections[0].Add(new Synapse(3, 2, _randomizer.NextDouble()));
        Connections[0].Add(new Synapse(3, 3, _randomizer.NextDouble()));
        Connections[0].Add(new Synapse(3, 4, _randomizer.NextDouble()));

        Connections[0].Add(new Synapse(4, 3, _randomizer.NextDouble()));
        Connections[0].Add(new Synapse(4, 4, _randomizer.NextDouble()));
        Connections[0].Add(new Synapse(4, 5, _randomizer.NextDouble()));
        Connections[0].Add(new Synapse(4, 6, _randomizer.NextDouble()));

        Connections[0].Add(new Synapse(5, 4, _randomizer.NextDouble()));
        Connections[0].Add(new Synapse(5, 5, _randomizer.NextDouble()));
        Connections[0].Add(new Synapse(5, 6, _randomizer.NextDouble()));

        //Connections[0].Add(new Synapse(6, 5, random.NextDouble()));
        //Connections[0].Add(new Synapse(6, 6, random.NextDouble()));
        //Connections[0].Add(new Synapse(6, 7, random.NextDouble()));

        //Connections[0].Add(new Synapse(7, 6, random.NextDouble()));
        //Connections[0].Add(new Synapse(7, 7, random.NextDouble()));
        //Connections[0].Add(new Synapse(7, 8, random.NextDouble()));
        //Connections[0].Add(new Synapse(7, 9, random.NextDouble()));

        //Connections[0].Add(new Synapse(8, 7, random.NextDouble()));
        //Connections[0].Add(new Synapse(8, 8, random.NextDouble()));
        //Connections[0].Add(new Synapse(8, 9, random.NextDouble()));

        Connections[1].Add(new Synapse(0, 0, _randomizer.NextDouble()));
        Connections[1].Add(new Synapse(1, 0, _randomizer.NextDouble()));
        Connections[1].Add(new Synapse(2, 0, _randomizer.NextDouble()));
        Connections[1].Add(new Synapse(2, 1, _randomizer.NextDouble()));
        Connections[1].Add(new Synapse(3, 0, _randomizer.NextDouble()));
        Connections[1].Add(new Synapse(3, 1, _randomizer.NextDouble()));
        Connections[1].Add(new Synapse(4, 0, _randomizer.NextDouble()));
        Connections[1].Add(new Synapse(4, 1, _randomizer.NextDouble()));
        Connections[1].Add(new Synapse(5, 1, _randomizer.NextDouble()));
        Connections[1].Add(new Synapse(6, 1, _randomizer.NextDouble()));
        //Connections[1].Add(new Synapse(7, 0, random.NextDouble()));
        //Connections[1].Add(new Synapse(7, 1, random.NextDouble()));
        //Connections[1].Add(new Synapse(8, 0, random.NextDouble()));
        //Connections[1].Add(new Synapse(8, 1, random.NextDouble()));
        //Connections[1].Add(new Synapse(9, 1, random.NextDouble()));

        foreach (var synapse in Connections[0])
        {
            InputLayer.Neurons[synapse.InIndex].Outputs.Add(synapse);
            HiddenLayer.Neurons[synapse.OutIndex].Inputs.Add(synapse);
        }

        foreach (var synapse in Connections[1])
        {
            HiddenLayer.Neurons[synapse.InIndex].Outputs.Add(synapse);
            OutputLayer.Neurons[synapse.OutIndex].Inputs.Add(synapse);
        }
    }

    public override object Clone()
    {
        AnimalBrain clonedNetwork = new();

        for (int i = 0; i < InputsCount; i++)
        {
            clonedNetwork.InputLayer.Neurons[i] = InputLayer.Neurons[i].Clone() as EvolvingNeuron;
            clonedNetwork.InputLayer.Neurons[i].Inputs.Add(new Synapse(i, i, 1));
        }

        for (int i = 0; i < HiddenCount; i++)
        {
            clonedNetwork.HiddenLayer.Neurons[i] = HiddenLayer.Neurons[i].Clone() as EvolvingNeuron;
        }

        for (int i = 0; i < OutputsCount; i++)
        {
            clonedNetwork.OutputLayer.Neurons[i] = OutputLayer.Neurons[i].Clone() as EvolvingNeuron;
            clonedNetwork.OutputLayer.Neurons[i].Outputs.Add(new Synapse(i, i, 1));
        }

        Connections[0].ForEach(s => clonedNetwork.Connections[0].Add(s.Clone() as Synapse));
        Connections[1].ForEach(s => clonedNetwork.Connections[1].Add(s.Clone() as Synapse));

        foreach (var synapse in clonedNetwork.Connections[0])
        {
            clonedNetwork.InputLayer.Neurons[synapse.InIndex].Outputs.Add(synapse);
            clonedNetwork.HiddenLayer.Neurons[synapse.OutIndex].Inputs.Add(synapse);
        }

        foreach (var synapse in clonedNetwork.Connections[1])
        {
            clonedNetwork.HiddenLayer.Neurons[synapse.InIndex].Outputs.Add(synapse);
            clonedNetwork.OutputLayer.Neurons[synapse.OutIndex].Inputs.Add(synapse);
        }

        return clonedNetwork;
    }
}
