﻿using LifeDemo.NeuralNet;
using LifeDemo.Simulation;
using SciML.GeneticAlgorithm;

namespace LifeDemo.Evolution;

public class PredatorFitness : IFitness<PredatorBrain, double>
{
    private readonly int _width;
    private readonly int _height;
    private readonly int _entitiesCount;
    private readonly int _foodCount;
    private readonly int _tournamentRounds;

    public PredatorFitness()
    {
        _width = 400;
        _height = 400;
        _entitiesCount = 5;
        _foodCount = 5;
        _tournamentRounds = 100;
    }

    public double Calculate(PredatorBrain chromosome)
    {
        EvolutionEnvironment env = new(_width, _height);

        for (int i = 0; i < _entitiesCount; i++)
        {
            env.InitializePredators(chromosome.Clone() as PredatorBrain, 1);
        }

        for (int i = 0; i < _foodCount; i++)
        {
            env.InitializeAnimals(AnimalBrain.GetRandomBrain(), 1);
        }

        EatenAnimalObserver tournamentListener = new();
        env.OnUpdate += tournamentListener.Notify;

        for (int i = 0; i < _tournamentRounds; i++)
        {
            env.Iterate();
        }

        double score = tournamentListener.Score;
        return score;
    }
}
