﻿using LifeDemo.Simulation;
using SciML.NeuralNetwork.Evolution;
using SciSim.Core.Simulation;
using System.Collections.Generic;

namespace LifeDemo.Evolution;

public class EatenAnimalObserver
{
    protected const double MinEatDistance = 5;
    protected const double MaxFishesDistance = 5;

    private double score = 0;

    public double Score => score < 0 ? 0 : score;

    public void Notify<T>(Environment<T> environment) where T : Environment<T>
    {
        var env = environment as EvolutionEnvironment;
        List<Animal> eatenFood = GetEatenFood(env);
        score += eatenFood.Count;

        List<Predator> collidedFishes = GetCollidedFishes(env);
        score -= collidedFishes.Count * 0.5;

        RemoveEatenAndCreateNewFood(env, eatenFood);
    }

    protected void RemoveEatenAndCreateNewFood(EvolutionEnvironment env, List<Animal> eatenFood)
    {
        foreach (Animal food in eatenFood)
        {
            env.RemoveEntity(food);
            AddRandomPieceOfFood(env);
        }
    }

    protected virtual void AddRandomPieceOfFood(EvolutionEnvironment env)
    {
        if (env.RegenerateFood)
        {
            env.InitializeAnimals(env.FilterBy<Animal>()[0].Brain.Clone() as EvolvingNetBase, 1);
        }
    }

    private static List<Predator> GetCollidedFishes(EvolutionEnvironment env)
    {
        List<Predator> collidedFishes = new();

        List<Predator> allFishes = env.FilterBy<Predator>();
        int fishesCount = allFishes.Count;

        for (int i = 0; i < (fishesCount - 1); i++)
        {
            Predator firstFish = allFishes[i];

            for (int j = i + 1; j < fishesCount; j++)
            {
                Predator secondFish = allFishes[j];
                double distanceToSecondFish = env.DistanceBetween(firstFish, secondFish);

                if (distanceToSecondFish < MaxFishesDistance)
                {
                    collidedFishes.Add(secondFish);
                }
            }
        }

        return collidedFishes;
    }

    private static List<Animal> GetEatenFood(EvolutionEnvironment env)
    {
        List<Animal> eatenFood = new();

        foreach (Animal food in env.FilterBy<Animal>())
        {
            foreach (Predator fish in env.FilterBy<Predator>())
            {
                double distanceToFood = env.DistanceBetween(food, fish);

                if (distanceToFood < MinEatDistance)
                {
                    eatenFood.Add(food);
                    break;
                }
            }
        }

        return eatenFood;
    }
}
