﻿using LifeDemo.NeuralNet;
using LifeDemo.Simulation;
using SciML.GeneticAlgorithm;

namespace LifeDemo.Evolution;

public class AnimalFitness : IFitness<AnimalBrain, double>
{
    private readonly int _width;
    private readonly int _height;
    ////private readonly int _predatorsCount;
    private readonly int _animalsCount;
    private readonly int _foodCount;
    private readonly int _tournamentRounds;

    public AnimalFitness()
    {
        _width = 200;
        _height = 200;
        ////_predatorsCount = 5;
        _animalsCount = 5;
        _foodCount = 5;
        _tournamentRounds = 100;
    }

    public double Calculate(AnimalBrain chromosome)
    {
        EvolutionEnvironment env = new(_width, _height);

        //for (int i = 0; i < predatorsCount; i++)
        //{
        //    env.InitializePredators(PredatorBrain.GetRandomBrain(), 1);
        //}

        for (int i = 0; i < _animalsCount; i++)
        {
            Animal animal = env.CreateRandomAnimal(chromosome.Clone() as AnimalBrain, _width, _height);
            env.AddEntity(animal);
        }

        env.InitializeFood(_foodCount);

        EatenFoodObserver eatenFoodListener = new();
        env.OnUpdate += eatenFoodListener.Notify;

        //var eatenAnimalsListener = new EatenAnimalObserver();
        //env.OnUpdate += eatenAnimalsListener.Notify;

        for (int i = 0; i < _tournamentRounds; i++)
        {
            env.Iterate();
        }

        //double foodScore = eatenFoodListener.Score;
        //double animalsScore = eatenAnimalsListener.Score;

        //return foodScore - animalsScore / 2;

        return eatenFoodListener.Score;
    }
}
