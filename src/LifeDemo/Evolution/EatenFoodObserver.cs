﻿using LifeDemo.Simulation;
using SciSim.Core.Simulation;
using System.Collections.Generic;
using System.Linq;

namespace LifeDemo.Evolution;

public class EatenFoodObserver
{
    protected const double MinEatDistance = 5;
    protected const double MaxFishesDistance = 5;

    private double score = 0;

    public double Score => score < 0 ? 0 : score;

    public void Notify<T>(Environment<T> environment) where T : Environment<T>
    {
        var env = environment as EvolutionEnvironment;

        List<Food> eatenFood = GetEatenFood(env);
        score += eatenFood.Count();

        List<Animal> collidedFishes = GetCollidedFishes(env);
        score -= collidedFishes.Count * 0.5;

        RemoveEatenAndCreateNewFood(env, eatenFood);
    }

    protected void RemoveEatenAndCreateNewFood(EvolutionEnvironment env, List<Food> eatenFood)
    {
        foreach (Food food in eatenFood)
        {
            env.RemoveEntity(food);

            AddRandomPieceOfFood(env);
        }
    }

    protected virtual void AddRandomPieceOfFood(EvolutionEnvironment env)
    {
        if (env.RegenerateFood)
        {
            env.InitializeFood(1);
        }
    }

    private static List<Animal> GetCollidedFishes(EvolutionEnvironment env)
    {
        List<Animal> collidedFishes = new();

        List<Animal> allFishes = env.FilterBy<Animal>();
        int fishesCount = allFishes.Count;

        for (int i = 0; i < (fishesCount - 1); i++)
        {
            Animal firstFish = allFishes[i];
            for (int j = i + 1; j < fishesCount; j++)
            {
                Animal secondFish = allFishes[j];
                double distanceToSecondFish = env.DistanceBetween(firstFish, secondFish);
                if (distanceToSecondFish < MaxFishesDistance)
                {
                    collidedFishes.Add(secondFish);
                }
            }
        }
        return collidedFishes;
    }

    private static List<Food> GetEatenFood(EvolutionEnvironment env)
    {
        List<Food> eatenFood = new();

        foreach (Food food in env.FilterBy<Food>())
        {
            foreach (Animal fish in env.FilterBy<Animal>())
            {
                double distanceToFood = env.DistanceBetween(food, fish);
                if (distanceToFood < MinEatDistance)
                {
                    eatenFood.Add(food);
                    break;
                }
            }
        }
        return eatenFood;
    }
}
