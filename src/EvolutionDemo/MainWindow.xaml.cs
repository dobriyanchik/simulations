﻿using EvolutionDemo.Simulation;
using EvolutionDemo.Visualization;
using MersenneTwister;
using SciML.GeneticAlgorithm;
using SciML.GeneticAlgorithm.Evolution;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace EvolutionDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int DelayTime = 1;


        private EvolutionEngine<Creature, double> ee;

        private EvolutionEnvironment environment;
        private Random random = Randoms.WellBalanced;//new Random();
        private int populationNumber = 0;
        private volatile bool play = true;
        //private Timer t;
        //private Timer tDraw;

        private CancellationTokenSource tokenSource;

        public MainWindow()
        {
            InitializeComponent();

            viewport.ClipToBounds = true;

            int parentalChromosomesSurviveCount = 1;
            int animalsCount = 10;
            int foodCount = 10;

            initializeGeneticAlgorithm(animalsCount, parentalChromosomesSurviveCount);

            initializeEnvironment((int)viewport.Width, (int)viewport.Height, animalsCount, foodCount);

            radioStatic.IsChecked = true;
            checkboxRegenerate.IsChecked = true;

            tokenSource = new CancellationTokenSource();

            new Task(() => EnvironmentStep(), tokenSource.Token).Start();
            new Task(() => RedrawSimulation(), tokenSource.Token).Start();
        }

        private void EnvironmentStep()
        {
            while (true)
            {
                if (play)
                {
                    environment.Iterate();
                    Task.Delay(DelayTime).Wait();
                }
            }
        }

        private void RedrawSimulation()
        {
            while (true)
            {
                if (play)
                {
                    viewport.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => EvolutionVisualizer.DrawEnvironment(environment, viewport)));
                    //EvolutionVisualizer.DrawEnvironment(environment, viewport);
                }
            }
        }


        private void initializeEnvironment(int environmentWidth, int environmentHeight, int agentsCount, int foodCount)
        {
            environment = new EvolutionEnvironment(environmentWidth, environmentHeight, ee);
            environment.OnUpdate += new EatenFoodObserver().Notify;
            environment.InitializeFood(foodCount);
        }

        private void initializeGeneticAlgorithm(int populationSize, int parentalChromosomesSurviveCount)
        {
            Population<Creature> creatures = new Population<Creature>();

            for (int i = 0; i < populationSize; i++)
            {
                int x = random.Next((int)viewport.Width);
                int y = random.Next((int)viewport.Height);
                double direction = random.NextDouble() * 2 * Math.PI;
                RootCreature creature = new RootCreature(x, y, direction);

                creatures.AddChromosome(creature);
            }

            CreatureFitness fit = new CreatureFitness();

            ee = new EvolutionEngine<Creature, double>(creatures, fit);
            ee.ParentChromosomesSurviveCount = parentalChromosomesSurviveCount;
        }

        private void SetControlsState(bool isEnabled)
        {
            buttonEvolve.IsEnabled = isEnabled;
            buttonPause.IsEnabled = isEnabled;
            buttonReset.IsEnabled = isEnabled;
            textboxEvolve.IsEnabled = isEnabled;
            radioStatic.IsEnabled = isEnabled;
            radioDynamic.IsEnabled = isEnabled;
            checkboxRegenerate.IsEnabled = isEnabled;
        }

        private void buttonReset_Click(object sender, RoutedEventArgs e)
        {
            SetControlsState(false);

            int populationSize = ee.Population.Size;
            int parentalChromosomesSurviveCount = ee.ParentChromosomesSurviveCount;
            initializeGeneticAlgorithm(populationSize, parentalChromosomesSurviveCount);

            // reset population number counter
            populationNumber = 0;
            labelPopulation.Content = "Population: " + populationNumber;

            SetControlsState(true);
        }

        private void buttonPause_Click(object sender, RoutedEventArgs e)
        {
            play = !play;
            buttonPause.Content = play ? "Pause" : "Play";
        }

        private void buttonEvolve_Click(object sender, RoutedEventArgs e)
        {
            play = false;

            SetControlsState(false);

            //int iterCount = Convert.ToInt32(textboxEvolve.Text);

            ee.Evolve();

            //populationNumber += iterCount;
            populationNumber++;
            labelPopulation.Content = "Population: " + populationNumber;
            SetControlsState(true);

            int total = ee.Population.Size;
            int root = ee.Population.FilterChromosomes<RootCreature>().Count;
            int l1 = ee.Population.FilterChromosomes<L1Creature>().Count;
            int l2 = ee.Population.FilterChromosomes<L2Creature>().Count;

            string text = $"Total: {total}\nRoot creatures: {root}\nL1 creatures: {l1}\nL2 creatures: {l2}";

            statsTbox.Text = text;

            play = true;
        }

        private void radioStatic_Checked(object sender, RoutedEventArgs e)
        {
            SetControlsState(false);
            bool wasPlaying = play;
            play = false;

            environment.ChangeFoodType(true);

            play = wasPlaying;

            SetControlsState(true);
        }

        private void radioDynamic_Checked(object sender, RoutedEventArgs e)
        {
            SetControlsState(false);
            bool wasPlaying = play;
            play = false;

            environment.ChangeFoodType(false);

            play = wasPlaying;

            SetControlsState(true);
        }

        private void checkboxRegenerate_Checked(object sender, RoutedEventArgs e) =>
            environment.RegenerateFood = true;

        private void checkboxRegenerate_Unchecked(object sender, RoutedEventArgs e) =>
            environment.RegenerateFood = false;

        private void viewport_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Point p = e.GetPosition(viewport);

            Food food = environment.CreateRandomFood(1, 1);
            food.Location.X = p.X;
            food.Location.Y = p.Y;
            environment.AddFood(food);
        }

        private void viewport_MouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            //Point p = e.GetPosition(viewport);
            //double angle = 2 * Math.PI * random.NextDouble();
            //RootCreature agent = new RootCreature(p.X, p.Y, angle);

            //environment.AddEntity(agent);
        }
    }
}
