﻿using SciSim.Core.Math;
using SciSim.Core.Simulation;

namespace EvolutionDemo.Simulation
{
    public class Food : IEntity
    {
        public Food(double x, double y)
        {
            Location = new Vector2D(x, y);
        }

        public Vector2D Location { get; set; }

        public virtual void InteractWith<T>(T environment) where T : Environment<T>
        {
        }
    }
}
