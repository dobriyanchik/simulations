﻿using SciSim.Core.Simulation;
using System.Collections.Generic;
using System.Linq;

namespace EvolutionDemo.Simulation
{
    public class EatenFoodObserver
    {
        protected const double MinEatDistance = 5;
        protected const double MaxFishesDistance = 5;

        private double score = 0;

        public double Score => score < 0 ? 0 : score;

        public void Notify<T>(Environment<T> environment) where T : Environment<T>
        {
            EvolutionEnvironment env = environment as EvolutionEnvironment;

            List<Food> eatenFood = GetEatenFood(env);
            score += eatenFood.Count();

            List<Creature> collidedFishes = GetCollidedFishes(env);
            score -= collidedFishes.Count * 0.5;

            RemoveEatenAndCreateNewFood(env, eatenFood);
        }

        protected void RemoveEatenAndCreateNewFood(EvolutionEnvironment env, List<Food> eatenFood)
        {
            foreach (Food food in eatenFood)
            {
                env.RemoveFood(food);
                AddRandomPieceOfFood(env);
            }
        }

        protected virtual void AddRandomPieceOfFood(EvolutionEnvironment env)
        {
            if (env.RegenerateFood)
            {
                env.InitializeFood(1);
            }
        }

        private List<Creature> GetCollidedFishes(EvolutionEnvironment env)
        {
            List<Creature> collidedFishes = new List<Creature>();

            List<Creature> allFishes = env.Engine.Population.FilterChromosomes<Creature>();
            int fishesCount = allFishes.Count;

            for (int i = 0; i < (fishesCount - 1); i++)
            {
                Creature firstFish = allFishes[i];
                for (int j = i + 1; j < fishesCount; j++)
                {
                    Creature secondFish = allFishes[j];
                    double distanceToSecondFish = env.DistanceBetween(firstFish, secondFish);
                    if (distanceToSecondFish < MaxFishesDistance)
                    {
                        collidedFishes.Add(secondFish);
                    }
                }
            }
            return collidedFishes;
        }

        private List<Food> GetEatenFood(EvolutionEnvironment env)
        {
            List<Food> eatenFood = new List<Food>();

            foreach (Food food in env.Food)
            {
                foreach (Creature fish in env.Engine.Population.FilterChromosomes<Creature>())
                {
                    double distanceToFood = env.DistanceBetween(food, fish);

                    if (distanceToFood < MinEatDistance)
                    {
                        eatenFood.Add(food);
                        break;
                    }
                }
            }
            return eatenFood;
        }
    }
}
