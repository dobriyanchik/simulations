﻿using System;
using System.Collections.Generic;

namespace EvolutionDemo.Simulation
{
    public class RootCreature : Creature
    {
        private const double MaxAnimalSpeed = 2;
        protected const double MaxAnimalsDistance = 10;
        protected const double MaxDistanceToPredator = 200;

        public RootCreature(double x, double y, double angle) : base(x, y, angle) { }

        protected override double MaxSpeed => MaxAnimalSpeed;

        public override double SpontaneousBirthRate { get; set; } = 1;

        public override double SpontaneousDeathRate { get; set; } = 0.1;

        public override double MutationRate { get; set; } = 0.1;

        public override double ReplicationRate { get; set; } = 0.05;

        public override Creature Clone() =>
            new RootCreature(Location.X, Location.Y, Angle);

        public override List<Creature> Crossover(Creature anotherChromosome) =>
            throw new NotImplementedException();

        public override Creature Mutate() =>
            new L1Creature(Location.X, Location.Y, Angle);
    }
}
