﻿using MersenneTwister;
using SciML.GeneticAlgorithm.Evolution;
using SciSim.Core.Simulation;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace EvolutionDemo.Simulation
{
    public class EvolutionEnvironment : Environment<EvolutionEnvironment>
    {
        private readonly Random _random;
        private volatile bool staticFood = true;
        private volatile bool regenerateFood = true;

        public EvolutionEnvironment(int width, int height, EvolutionEngine<Creature, double> ee) : base(width, height)
        {
            Engine = ee;
            _random = Randoms.WellBalanced;//new Random();
        }

        public EvolutionEngine<Creature, double> Engine { get; set; }

        public bool RegenerateFood
        {
            get => regenerateFood;

            set => regenerateFood = value;
        }

        // Was LinkedList to avoid concurrent modification exception.
        public List<Food> Food { get; } = new List<Food>();

        protected override void TimeStep()
        {
            foreach (Food entity in Food)
            {
                entity.InteractWith(this);
                AvoidMovingOutsideOfBounds(entity);
            }

            foreach (Creature entity in Engine.Population.FilterChromosomes<Creature>())
            {
                entity.InteractWith(this);
                AvoidMovingOutsideOfBounds(entity);
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void AddFood(Food food) =>
            Food.Add(food);

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void RemoveFood(Food food) =>
            Food.Remove(food);

        public void InitializeFood(int foodCount)
        {
            for (int i = 0; i < foodCount; i++)
            {
                Food food = CreateRandomFood(Width, Height);
                AddFood(food);
            }
        }

        public Food CreateRandomFood(int width, int height)
        {
            int x = _random.Next(width);
            int y = _random.Next(height);

            if (staticFood)
            {
                return new Food(x, y);
            }
            else
            {
                double speed = _random.NextDouble();
                double direction = _random.NextDouble() * 2 * Math.PI;

                return new MovingFood(x, y, direction, speed);
            }
        }

        public void ChangeFoodType(bool isStaticFood)
        {
            staticFood = isStaticFood;

            int count = Food.Count;

            for (int i = 0; i < count; i++)
            {
                Food oldFood = Food[0];

                Food newFood = CreateRandomFood(1, 1);
                newFood.Location.X = oldFood.Location.X;
                newFood.Location.Y = oldFood.Location.Y;

                RemoveFood(oldFood);
                AddFood(newFood);
            }
        }
    }
}
