﻿using System;
using System.Collections.Generic;

namespace EvolutionDemo.Simulation
{
    public class L2Creature : Creature
    {
        private const double MaxAnimalSpeed = 2;
        protected const double MaxAnimalsDistance = 10;
        protected const double MaxDistanceToPredator = 200;

        public L2Creature(double x, double y, double angle) : base(x, y, angle) { }

        protected override double MaxSpeed => MaxAnimalSpeed;

        public override double SpontaneousBirthRate { get; set; } = 0;

        public override double SpontaneousDeathRate { get; set; } = 0.05;

        public override double MutationRate { get; set; } = 0;

        public override double ReplicationRate { get; set; } = 0.1;

        public override Creature Clone() =>
            new L2Creature(Location.X, Location.Y, Angle);

        public override List<Creature> Crossover(Creature anotherChromosome) =>
            throw new NotImplementedException();

        public override Creature Mutate() =>
            throw new NotImplementedException();
    }
}
