﻿using SciSim.Core.Math;
using SciSim.Core.Simulation;
using System;

namespace EvolutionDemo.Simulation
{
    public class MovingFood : Food, IMovingEntity
    {
        private readonly Vector2D _movement;

        public MovingFood(double x, double y, double angle, double speed) : base(x, y)
        {
            Speed = speed;
            Angle = angle;
            MoveDirection.X = -Math.Sin(Angle);
            MoveDirection.Y = Math.Cos(Angle);

            _movement = MoveDirection * Speed;
        }

        public Vector2D MoveDirection { get; set; }

        public double Angle { get; set; }

        public double Speed { get; set; }

        public override void InteractWith<T>(T environment) =>
            Move();

        public void Move() =>
            Location += _movement;
    }
}
