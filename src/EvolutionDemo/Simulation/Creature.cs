﻿using MersenneTwister;
using SciML.GeneticAlgorithm.Evolution;
using SciSim.Core.Math;
using SciSim.Core.Simulation;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace EvolutionDemo.Simulation
{
    public abstract class Creature : IEntity, ICreature<Creature>
    {
        protected const double MaxDeltaAngle = 0.5;

        private readonly Random _randomizer = Randoms.FastestInt32;

        public Creature(double x, double y, double angle)
        {
            Location = new Vector2D(x, y);
            Speed = 0;
            Angle = angle;
            CalculateR();
        }

        public abstract double SpontaneousBirthRate { get; set; }

        public abstract double SpontaneousDeathRate { get; set; }

        public abstract double MutationRate { get; set; }

        public abstract double ReplicationRate { get; set; }

        public Vector2D Location { get; set; }

        public double Angle { get; set; }

        public double Speed { get; set; }

        public Vector2D R { get; set; }

        protected abstract double MaxSpeed { get; }

        public void Move() =>
            Location += R * Speed;

        /// <summary>
        /// Synchronization prevents from race condition when trying to set new brain, while method "interact" runs<br/><br/>
        /// TODO Maybe consider to use non-blocking technique. But at the moment this simplest solution doesn't cause any overheads
        /// </summary>
        /// <param name="env"></param>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void InteractWith<T>(T environment) where T : Environment<T>
        {
            double deltaAngle = _randomizer.Next(-200, 200) / 100d;
            double deltaSpeed = _randomizer.Next(0, 200) / 100d;

            deltaSpeed = Adjustment.AvoidNaNAndInfinity(deltaSpeed);
            deltaAngle = Adjustment.AvoidNaNAndInfinity(deltaAngle);

            double newSpeed = Adjustment.NormalizeValue(Speed + deltaSpeed, MaxSpeed);
            double newAngle = Angle + Adjustment.NormalizeValue(deltaAngle, MaxDeltaAngle);

            Angle = newAngle;
            Speed = newSpeed;
            CalculateR();

            Move();
        }

        public abstract Creature Clone();

        public abstract List<Creature> Crossover(Creature anotherChromosome);

        public abstract Creature Mutate();

        protected void CalculateR()
        {
            R.X = -Math.Sin(Angle);
            R.Y = Math.Cos(Angle);
        }
    }
}
