﻿using EvolutionDemo.Simulation;
using System;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EvolutionDemo.Visualization
{
    public static class EvolutionVisualizer
    {
        private const double AnimalRadius = 10;
        private const double AnimalRadiusMul2 = AnimalRadius * 2;
        private const double AnimalRadiusAdd4 = AnimalRadius + 4;

        private static Brush entityPen = Brushes.White;

        private static readonly ImageBrush entitySkin = new ImageBrush(new BitmapImage(new Uri(@"Resources\entity.png", UriKind.Relative)));
        private static readonly ImageBrush entity1Skin = new ImageBrush(new BitmapImage(new Uri(@"Resources\entity1.png", UriKind.Relative)));
        private static readonly ImageBrush entity2Skin = new ImageBrush(new BitmapImage(new Uri(@"Resources\entity2.png", UriKind.Relative)));
        private static readonly ImageBrush foodSkin = new ImageBrush(new BitmapImage(new Uri(@"Resources\food.png", UriKind.Relative)));
        private static readonly BitmapImage background = new BitmapImage(new Uri(@"Resources\background.jpg", UriKind.Relative));

        public static void DrawEnvironment(EvolutionEnvironment environment, Canvas canvas)
        {
            canvas.Background = new ImageBrush(background);
            canvas.Children.Clear();
            DropShadowEffect shadow = new DropShadowEffect();

            foreach (Food food in environment.Food)
            {
                Ellipse ellipse = new Ellipse()
                {
                    Fill = foodSkin,
                    Width = AnimalRadiusMul2,
                    Height = AnimalRadiusMul2,
                    Effect = shadow
                };

                Canvas.SetLeft(ellipse, food.Location.X - AnimalRadius);
                Canvas.SetTop(ellipse, food.Location.Y - AnimalRadius);
                canvas.Children.Add(ellipse);
            }

            int agentToSelect = 0;

            foreach (Creature creature in environment.Engine.Population.FilterChromosomes<Creature>())
            {
                double x = creature.Location.X;
                double y = creature.Location.Y;

                Line line = new Line()
                {
                    X1 = x,
                    Y1 = y,
                    X2 = (creature.R.X * AnimalRadiusAdd4) + x,
                    Y2 = (creature.R.Y * AnimalRadiusAdd4) + y,
                    Stroke = entityPen,
                    StrokeThickness = 2
                };

                canvas.Children.Add(line);

                ImageBrush skin;

                if (creature is RootCreature)
                {
                    skin = entitySkin;
                }
                else if (creature is L1Creature)
                {
                    skin = entity1Skin;
                }
                else
                {
                    skin = entity2Skin;
                }

                Ellipse ellipse = new Ellipse()
                {
                    Fill = skin,
                    Width = AnimalRadiusMul2,
                    Height = AnimalRadiusMul2,
                    Effect = shadow
                };

                Canvas.SetLeft(ellipse, x - AnimalRadius);
                Canvas.SetTop(ellipse, y - AnimalRadius);
                canvas.Children.Add(ellipse);

                agentToSelect++;
            }
        }
    }

}
