﻿using Microsoft.Win32;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Imaging;

namespace NumbersRecognition
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Bitmap numberImage;
        private string numberImageFileName;
        private StringBuilder numberAscii;
        private NumbersRecognitionNeuralNet network;
        private const int XPixels = 3;
        private const int YPixels = 5;

        public MainWindow()
        {
            InitializeComponent();
            numberAscii = new StringBuilder();
            network = new NumbersRecognitionNeuralNet(XPixels * YPixels, 1);
        }

        private void BtnLoad_Click(object sender, RoutedEventArgs e)
        {
            var open = new OpenFileDialog();
            open.ShowDialog();

            this.numberImage = System.Drawing.Image.FromFile(open.FileName) as Bitmap;
            this.numberImageFileName = open.FileName.Split('\\').Last();

            using (MemoryStream memory = new MemoryStream())
            {
                numberImage.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                ImgNumber.Source = bitmapimage;
            }

            ProcessImage();
        }

        private void BtnAgain_Click(object sender, RoutedEventArgs e)
        {
            ProcessImage();
        }

        private void ProcessImage()
        {
            bool condition = numberImageFileName[0] == TBoxRecNum.Text.Trim()[0];

            var m = new int[XPixels, YPixels];

            for (int i = 0; i < XPixels; i++)
            {
                for (int j = 0; j < YPixels; j++)
                {
                    int pix = numberImage.GetPixel(i, j).R;
                    if (pix >= 250)
                    {
                        m[i, j] = 0;
                    }
                    else
                    {
                        m[i, j] = 1;
                    }
                }
            }

            network.ProcessMatrix(m);

            var mNew = network.InputsWeightsToMatrix(3, 5);

            numberAscii.Clear();

            for (int i = 0; i < YPixels; i++)
            {
                for (int j = 0; j < XPixels; j++)
                {
                    numberAscii.AppendFormat("{0, -4}", mNew[j, i]);
                }
                numberAscii.AppendLine();
            }
            numberAscii
                .AppendLine($"Sum = {network.OutputLayer.Neurons[0].Output.Signal}")
                .AppendLine($"Result = {network.Result}");
            TBoxNumberAscii.Text = numberAscii.ToString();

            if (!condition && network.Result)
            {
                network.DecreaseWeights();
            }
            else if (condition && !network.Result)
            {
                network.IncreaseWeights();
            }
        }
    }
}
