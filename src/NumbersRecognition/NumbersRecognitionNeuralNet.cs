﻿using SciML.NeuralNetwork.Entities;
using SciML.NeuralNetwork.Networks;
using System;

namespace NumbersRecognition
{
    public class NumbersRecognitionNeuralNet : NeuralNet2LayerBase<InputNeuron, OutputNeuron, Synapse>
    {
        private int treshhold = 9;
        private int inputNeurons;
        private int outputNeurons;

        public NumbersRecognitionNeuralNet(int inputNeurons, int outputNeurons)
            : base(inputNeurons, outputNeurons)
        {
            this.inputNeurons = inputNeurons;
            this.outputNeurons = outputNeurons;
            ConstructNetwork();
        }

        public bool Result { get; set; }

        public void ProcessMatrix(int[,] matrix)
        {
            PixelMatrixToSensors(matrix);
            Process();
            Result = OutputLayer.Neurons[0].Output.Signal > treshhold;
        }

        public int[,] InputsWeightsToMatrix(int x, int y)
        {
            int[,] m = new int[x, y];

            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    m[i, j] = (int)InputLayer.Neurons[i * j + j].Outputs[0].Weight;
                }
            }

            return m;
        }

        public void DecreaseWeights()
        {
            foreach (InputNeuron neuron in InputLayer.Neurons)
            {
                neuron.Outputs[0].Weight -= neuron.Input.Signal;
            }
        }

        public void IncreaseWeights()
        {
            foreach (InputNeuron neuron in InputLayer.Neurons)
            {
                neuron.Outputs[0].Weight += neuron.Input.Signal;
            }
        }

        public override void Process()
        {
            InputLayer.Process();
            OutputLayer.Process();
        }

        public override object Clone()
        {
            throw new NotImplementedException();
        }

        public override void ConstructNetwork()
        {
            inputNeurons = InputLayer.Neurons.Length;
            outputNeurons = OutputLayer.Neurons.Length;

            for (int i = 0; i < inputNeurons; i++)
            {
                InputLayer.Neurons[i] = new InputNeuron();
            }

            OutputLayer.Neurons[0] = new OutputNeuron();
            OutputLayer.Neurons[0].Output = new Synapse(0, 0);

            for (int i = 0; i < inputNeurons; i++)
            {
                InputLayer.Neurons[i].Input = new Synapse(i, i);

                var synapseOut = new Synapse(0, i);
                Connections.Add(synapseOut);
                InputLayer.Neurons[i].Outputs.Add(synapseOut);
                OutputLayer.Neurons[0].Inputs.Add(synapseOut);
            }
        }

        private void PixelMatrixToSensors(int[,] matrix)
        {
            int x = matrix.GetLength(0);
            int y = matrix.GetLength(1);

            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    InputLayer.Neurons[i * j + j].Input.Signal = matrix[i, j];
                }
            }
        }
    }
}
