﻿using RacingAI.NeuralNet;
using RacingAI.Simulation;
using SciML.GeneticAlgorithm;
using System.Linq;

namespace RacingAI.Evolution
{
    public class CarFitness : IFitness<CarProcessor, double>
    {
        private readonly RaceSimulation _simulation;

        public CarFitness(RaceSimulation simulation)
        {
            _simulation = simulation;
        }

        public double Calculate(CarProcessor chromosome)
        {
            var car = _simulation.Cars.First(c => c.Brain.Equals(chromosome));
            return car.CheckpointsPassed;
        }
    }
}
