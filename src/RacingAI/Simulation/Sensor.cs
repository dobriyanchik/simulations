﻿using SciSim.Core.Math;
using SciSim.Core.Physics;
using System;

namespace RacingAI.Simulation
{
    public class Sensor
    {
        private const double OptimizationPiDiv180 = Math.PI / 180;
        private readonly double _baseAngle;

        public Sensor(int index, double length)
        {
            Index = index;
            Length = length;
            _baseAngle = 165 - Index * 33f;
            Geometry = new CollidingSegment();
        }

        public int Index { get; }

        public double Length { get; }

        public CollidingSegment Geometry { get; }

        public void Recalculate(Vector2D location, double angle)
        {
            Geometry.V1 = location;

            double actualAngle = (_baseAngle - angle) * OptimizationPiDiv180;

            Geometry.V2.X = Geometry.V1.X + Math.Cos(actualAngle) * Length;
            Geometry.V2.Y = Geometry.V1.Y - Math.Sin(actualAngle) * Length;
        }

        public override string ToString() => 
            $"S{Index} {Geometry.V2.ToString("F0")}\t{Geometry.Collision.ToString("F0")}";
    }
}
