﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace RacingAI.Simulation
{
    public class RaceSimulation
    {
        public RaceSimulation(int carsCount, string trackName)
        {
            Track = new Track(trackName);
            Generation = 1;
            Cars = new List<Car>();

            for (int i = 0; i < carsCount; i++)
            {
                AddCar();
            }

            BestCar = Cars[0];
        }

        public int Generation { get; set; }

        public List<Car> Cars { get; protected set; }

        public Car BestCar { get; protected set; }

        public Track Track { get; protected set; }

        public void AddCar()
        {
            var newCar = new Car(Track.CarInitX, Track.CarInitY, Track.CarInitAngle);
            Cars.Add(newCar);
        }

        public void MakeKeyboardMovement() =>
            CheckKeysPressed();

        public void MakeNeuralMovement() =>
            Cars.ForEach(c => c.MakeNeuralMovement());

        public void UpdateCollisions()
        {
            Cars.ForEach(c => c.UpdateCollisions(this));
            BestCar = (from car in Cars orderby car.CheckpointsPassed descending select car).First();
        }

        public override string ToString()
        {
            StringBuilder log = new StringBuilder();

            log.AppendLine($"Generation {Generation} (best car)")
                .AppendLine()
                .AppendLine(BestCar.ToString());
            return log.ToString();
        }

        private void CheckKeysPressed()
        {
            if (Keyboard.IsKeyDown(Key.A))
            {
                if (Keyboard.IsKeyDown(Key.S))
                {
                    BestCar.TurnRight();
                }
                else
                {
                    BestCar.TurnLeft();
                }
            }

            if (Keyboard.IsKeyDown(Key.D))
            {
                if (Keyboard.IsKeyDown(Key.S))
                {
                    BestCar.TurnLeft();
                }
                else
                {
                    BestCar.TurnRight();
                }
            }

            if (Keyboard.IsKeyDown(Key.W))
            {
                BestCar.GoForward();
            }

            if (Keyboard.IsKeyDown(Key.S))
            {
                BestCar.GoBack();
            }
        }
    }
}
