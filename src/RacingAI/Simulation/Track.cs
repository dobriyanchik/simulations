﻿using SciSim.Core.Physics;
using System;
using System.Collections.Generic;

namespace RacingAI.Simulation
{
    public class Track
    {
        private List<Point> pointsLeft;
        private List<Point> pointsRight;

        public Track(string trackName)
        {
            Name = trackName;
            InitPoints(new Uri($@"Resources\Tracks\{trackName}.txt", UriKind.Relative));
            GenerateTrack();
        }

        public string Name { get; set; }

        public List<CollidingSegment> Borders { get; private set; }

        public List<CollidingSegment> Obstacles { get; private set; }

        public List<CollidingSegment> Checkpoints { get; private set; }

        public double CarInitX { get; private set; }

        public double CarInitY { get; private set; }

        public double CarInitAngle { get; private set; }

        private void InitPoints(Uri trackFile)
        {
            Obstacles = new List<CollidingSegment>();
            Borders = new List<CollidingSegment>();
            Checkpoints = new List<CollidingSegment>();

            pointsLeft = new List<Point>();
            pointsRight = new List<Point>();

            string[] pointPairs = System.IO.File.ReadAllLines(trackFile.ToString());

            string[] carInfo = pointPairs[0].Split('|');
            CarInitX = Convert.ToDouble(carInfo[0].Trim());
            CarInitY = Convert.ToDouble(carInfo[1].Trim());
            CarInitAngle = Convert.ToDouble(carInfo[2].Trim());

            for (int i = 1; i < pointPairs.Length; i++)
            {
                string pointPair = pointPairs[i].Trim();

                if (!string.IsNullOrEmpty(pointPair))
                {
                    if (pointPair.StartsWith("<") && pointPair.EndsWith(">"))
                    {
                        string[] pair = pointPair.TrimStart('<').TrimEnd('>').Split('|');

                        Obstacles.Add(new CollidingSegment(GetPoint(pair[0].Trim()), GetPoint(pair[1].Trim())));
                    }
                    else
                    {
                        string[] pair = pointPair.Split('|');
                        pointsLeft.Add(GetPoint(pair[0].Trim()));
                        pointsRight.Add(GetPoint(pair[1].Trim()));
                    }
                }
            }
        }

        private void GenerateTrack()
        {
            for (int i = 1; i < pointsLeft.Count; i++)
            {
                Borders.Add(new CollidingSegment(pointsLeft[i - 1], pointsLeft[i]));
                Borders.Add(new CollidingSegment(pointsRight[i - 1], pointsRight[i]));
                Checkpoints.Add(new CollidingSegment(pointsLeft[i], pointsRight[i]));
            }
        }

        private Point GetPoint(string coordinates)
        {
            string[] pair = coordinates.Split(',');

            return new Point(
                Convert.ToDouble(pair[0].Trim()),
                Convert.ToDouble(pair[1].Trim()));
        }
    }
}
