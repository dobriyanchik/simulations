﻿using RacingAI.NeuralNet;
using SciSim.Core.Math;
using SciSim.Core.Physics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace RacingAI.Simulation
{
    public class Car
    {
        public const double Height = 40, HalfHeight = Height / 2;
        public const double Width = Height / 2, HalfWidth = Width / 2;

        public const double SightRadius = Height * 3;
        public const int SensorsCount = 5;

        private const double MaxMoveStep = 3;
        private const double MaxDeltaAngle = 2;
        private const int CentralCensorIndex = SensorsCount / 2;

        private const double CollideExcitation = -10;
        private const double ZeroExcitation = 0;
        private const double CheckpointExcitation = 10;

        private const double OptimizedAngle = MaxMoveStep * 0.9f / 90;

        protected volatile CarProcessor brain;
        private readonly double _angleInit;
        private readonly Vector2D _initLocation;

        public Car(double xInit, double yInit, double angleInit)
        {
            _initLocation = new Vector2D(xInit, yInit);
            _angleInit = angleInit;

            Location = new Vector2D(xInit, yInit);
            Angle = angleInit;

            CheckpointsPassed = 0;

            Sensors = new Sensor[SensorsCount];
            InitSensors();
        }

        public CarProcessor Brain => brain;

        public Vector2D Location { get; }

        public double Angle { get; private set; }

        private double Speed { get; set; }

        public bool Stopped { get; protected set; } = false;

        public int CheckpointsPassed { get; set; }

        public bool CrossedCheckpoint { get; set; }

        public Sensor[] Sensors { get; private set; }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void SetBrain(CarProcessor brain) =>
            this.brain = brain;

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void MakeNeuralMovement()
        {
            if (Stopped)
            {
                return;
            }

            List<double> nnInputs = CalculateNeuralInputs();

            ActivateBrain(nnInputs);

            double deltaAngle = brain.OutputLayer.Neurons[0].Outputs[0].Signal;
            double deltaSpeed = brain.OutputLayer.Neurons[1].Outputs[0].Signal;

            deltaSpeed = Adjustment.AvoidNaNAndInfinity(deltaSpeed);
            deltaAngle = Adjustment.AvoidNaNAndInfinity(deltaAngle);

            Speed = Adjustment.NormalizeValue(Speed + deltaSpeed, MaxMoveStep);
            Angle += Adjustment.NormalizeValue(deltaAngle, MaxDeltaAngle);

            if (Angle < -180)
            {
                Angle += 360;
            }
            else if (Angle > 180)
            {
                Angle -= 360;
            }

            double ySign = -1;

            double calcAngle = Math.Abs(Angle);

            if (calcAngle > 90)
            {
                calcAngle = 180 - calcAngle;
                ySign = 1;
            }

            double optimizedAngle = Speed * 0.9d / 90;
            double yMove = Speed - Math.Abs(calcAngle) * optimizedAngle;
            double xMove = Speed - yMove;

            if (Speed > 0)
            {
                Location.Y += ySign * yMove;
                Location.X += Math.Sign(Angle) * xMove;
            }

            UpdateSensors();
        }

        public void Reset()
        {
            Location.X = _initLocation.X;
            Location.Y = _initLocation.Y;
            Angle = _angleInit;
            UpdateSensors();
            Stopped = false;
            CheckpointsPassed = 0;
        }

        public void Stop() =>
            Stopped = true;

        protected void ActivateBrain(List<double> nnInputs)
        {
            for (int i = 0; i < nnInputs.Count; i++)
            {
                brain.InputLayer.Neurons[i].Inputs[0].Signal = nnInputs[i];
            }

            brain.Process();
        }

        private List<double> CalculateNeuralInputs()
        {
            var neuralInputs = new List<double>();

            foreach (var sensor in Sensors)
            {
                if (sensor.Geometry.Collision.Collide)
                {
                    neuralInputs.Add(CollideExcitation * sensor.Geometry.Collision.Severity);
                }
                else if (sensor.Index == CentralCensorIndex && CrossedCheckpoint)
                {
                    neuralInputs.Add(CheckpointExcitation * sensor.Geometry.Collision.Severity);
                }
                else
                {
                    neuralInputs.Add(ZeroExcitation);
                }
            }

            return neuralInputs;
        }

        public void TurnLeft()
        {
            Angle -= MaxMoveStep;

            if (Angle < -180)
            {
                Angle += 360;
            }

            UpdateSensors();
        }

        public void TurnRight()
        {
            Angle += MaxMoveStep;

            if (Angle > 180)
            {
                Angle -= 360;
            }

            UpdateSensors();
        }

        public void GoForward()
        {
            int ySign = -1;

            double calcAngle = Math.Abs(Angle);

            if (calcAngle > 90)
            {
                calcAngle = 180 - calcAngle;
                ySign = 1;
            }

            ////float yMove = (1 - ((0.9f * Math.Abs(calcAngle)) / 90)) * this.moveStep;
            double yMove = MaxMoveStep - Math.Abs(calcAngle) * OptimizedAngle;
            double xMove = MaxMoveStep - yMove;

            Location.Y += ySign * yMove;
            Location.X += Math.Sign(Angle) * xMove;

            UpdateSensors();
        }

        public void GoBack()
        {
            int ySign = -1;

            double calcAngle = Math.Abs(Angle);

            if (calcAngle > 90)
            {
                calcAngle = 180 - calcAngle;
                ySign = 1;
            }

            ////float yMove = (1 - ((0.9f * Math.Abs(calcAngle)) / 90)) * this.moveStep;
            double yMove = MaxMoveStep - Math.Abs(calcAngle) * OptimizedAngle;
            double xMove = MaxMoveStep - yMove;

            Location.Y -= ySign * yMove;
            Location.X -= Math.Sign(Angle) * xMove;

            UpdateSensors();
        }

        public void UpdateCollisions(RaceSimulation simulation)
        {
            if (Stopped)
            {
                return;
            }

            CrossedCheckpoint = CheckForCrossedCheckpoint(simulation.Track);

            var obstacles = GetObstaclesInSight(simulation.Track);

            foreach (var sensor in Sensors)
            {
                if (sensor.Geometry.CollidesWithAny(obstacles))
                {
                    //var xDiff = (sensor.Geometry.X1 - sensor.Geometry.Collision.X);
                    //var yDiff = (sensor.Geometry.Y1 - sensor.Geometry.Collision.Y);
                    //double distanceFromCenter = xDiff * xDiff + yDiff * yDiff;
                    //distanceFromCenter = Math.Sqrt(distanceFromCenter);

                    double distanceFromCenter = 
                        Algebra.Distance(sensor.Geometry.V1, sensor.Geometry.Collision.Location);

                    sensor.Geometry.Collision.Severity = 1 - distanceFromCenter / sensor.Length;
                }
                else
                {
                    sensor.Geometry.Collision.Severity = 0;
                }
            }

            if (Sensors.Any(s => s.Geometry.Collision.Severity > 0.8))
            {
                Stop();
            }
        }

        private bool CheckForCrossedCheckpoint(Track track)
        {
            if (CheckpointsPassed == track.Checkpoints.Count)
            {
                return false;
            }

            var s = Sensors[CentralCensorIndex];
            if (s.Geometry.CollidesWithAny(track.Checkpoints[CheckpointsPassed]))
            {
                //var xDiff = (s.Geometry.X1 - s.Geometry.Collision.X);
                //var yDiff = (s.Geometry.Y1 - s.Geometry.Collision.Y);
                //double distanceFromCenter = xDiff * xDiff + yDiff * yDiff;
                //distanceFromCenter = Math.Sqrt(distanceFromCenter);

                double distanceFromCenter = Algebra.Distance(s.Geometry.V1, s.Geometry.Collision.Location);

                s.Geometry.Collision.Severity = 1 - distanceFromCenter / s.Length;

                if (s.Geometry.Collision.Severity > 0.7)
                {
                    if (CheckpointsPassed < track.Checkpoints.Count)
                    {
                        CheckpointsPassed++;
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            var log = new StringBuilder();

            log.Append($"{Location.ToString("F0")} {Angle:F0}°\n")
                .AppendLine($"Checkpoints: {CheckpointsPassed}")
                .AppendLine()
                .AppendLine("S  Position\t\tCollision>damage")
                .AppendLine(new string('-', 43));

            Array.ForEach(Sensors, s => log.AppendLine(s.ToString()));
            return log.ToString();
        }

        private CollidingSegment[] GetObstaclesInSight(Track track)
        {
            var segmentsInSight = new List<CollidingSegment>();

            for (int i = 0; i < track.Borders.Count; i++)
            {
                var border = track.Borders[i];

                if (Algebra.Distance(Location, border.V1) < SightRadius || 
                    Algebra.Distance(Location, border.V2) < SightRadius)
                {
                    segmentsInSight.Add(border);
                }
            }

            for (int i = 0; i < track.Obstacles.Count; i++)
            {
                var obstacle = track.Obstacles[i];

                if (Algebra.Distance(Location, obstacle.V1) < SightRadius ||
                    Algebra.Distance(Location, obstacle.V2) < SightRadius)
                {
                    segmentsInSight.Add(obstacle);
                }
            }

            return segmentsInSight.ToArray();
        }

        private void UpdateSensors() =>
            Array.ForEach(Sensors, s => s.Recalculate(Location, Angle));

        private void InitSensors()
        {
            for (int i = 0; i < SensorsCount; i++)
            {
                var multiplier = i == CentralCensorIndex ? 2 : 1.5;
                var sensor = new Sensor(i, Height * multiplier);
                sensor.Recalculate(Location, Angle);
                Sensors[i] = sensor;
            }
        }
    }
}
