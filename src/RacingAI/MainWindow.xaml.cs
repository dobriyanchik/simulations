﻿using Microsoft.Win32;
using RacingAI.Evolution;
using RacingAI.NeuralNet;
using RacingAI.Simulation;
using RacingAI.Visualization;
using SciML.GeneticAlgorithm;
using SciML.NeuralNetwork.Evolution;
using SciSim.NNVisualization;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace RacingAI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int DelayTime = 1;
        private const int CarsCount = 5;

        private Uri brainsDir = new Uri("Brains", UriKind.Relative);

        private RaceSimulation simulation;
        private EvolvingNeuralNetVisualizer brainVisualizator;
        private EvolutionVisualizer evolutionVisualizator;
        private GeneticEngine<CarProcessor, double> geneticEngine;

        private CancellationTokenSource tokenSource;

        private CarProcessor loadedProcessor = null;

        private bool forceEvolve = false;
        private bool neural;

        public MainWindow()
        {
            InitializeComponent();
            brainVisualizator = new EvolvingNeuralNetVisualizer(viewportBrain);
            labelBaseBrain.Content = "Base brain:\nrandom";
        }

        string TrackName => (comboTracks.SelectedItem as ComboBoxItem).Content.ToString();

        private void buttonStartSimulation_Click(object sender, RoutedEventArgs e)
        {
            neural = false;
            simulation = new RaceSimulation(1, TrackName);

            tokenSource?.Cancel();
            tokenSource?.Dispose();
            tokenSource = new CancellationTokenSource();


            var scheduler = TaskScheduler.FromCurrentSynchronizationContext();

            Thread t = new Thread(() => SimulateNextFrame());
            t.SetApartmentState(ApartmentState.STA);
            t.Start();

            //Task.Factory.StartNew(() => SimulateNextFrame(), tokenSource.Token);
            //.ContinueWith(r => AddControlsToGrid(r.Result), scheduler);

            //new Task(() => SimulateNextFrame(), tokenSource.Token).Start();
            new Task(() => LogEvents(), tokenSource.Token).Start();

            InitDrawing(tokenSource.Token);
        }

        private void buttonStartSimulationAI_Click(object sender, RoutedEventArgs e)
        {
            neural = false;
            simulation = new RaceSimulation(1, TrackName);
            var brain = loadedProcessor ?? CarProcessor.GetRandomProcessor();
            simulation.Cars[0].SetBrain(brain);

            tokenSource?.Cancel();
            tokenSource?.Dispose();
            tokenSource = new CancellationTokenSource();

            new Task(() => SimulateNextAiFrame(), tokenSource.Token).Start();
            new Task(() => LogEvents(), tokenSource.Token).Start();

            InitDrawing(tokenSource.Token);
        }

        private void buttonStartTraining_Click(object sender, RoutedEventArgs e)
        {
            neural = true;
            simulation = new RaceSimulation(CarsCount, TrackName);

            InitGeneticAlgorithm(CarsCount, 1, loadedProcessor);

            tokenSource?.Cancel();
            tokenSource?.Dispose();
            tokenSource = new CancellationTokenSource();

            new Task(() => SimulateNextTrainingFrame(), tokenSource.Token).Start();
            new Task(() => LogEvents(), tokenSource.Token).Start();

            InitDrawing(tokenSource.Token);
        }

        private void InitDrawing(CancellationToken token)
        {
            evolutionVisualizator = new EvolutionVisualizer(simulation, viewportTrack, viewport);
            new Task(() => DrawNextFrame(), token).Start();
        }

        private void CheckForCompletion()
        {
            if (simulation.BestCar.CheckpointsPassed == simulation.Track.Checkpoints.Count)
            {
                tokenSource.Cancel();
                MessageBox.Show("Training on the track completed");
            }
        }

        private void DrawNextFrame()
        {
            while (true)
            {
                viewport.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => evolutionVisualizator.DrawNextFrame()));

                if (neural)
                {
                    viewportBrain.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => brainVisualizator.DrawBrain(simulation.BestCar.Brain)));
                }
            }
        }

        private void SimulateNextFrame()
        {
            while (true)
            {
                if (!simulation.Cars[0].Stopped)
                {
                    simulation.MakeKeyboardMovement();
                    simulation.UpdateCollisions();
                }
                else
                {
                    simulation.Cars[0].Reset();
                }

                Task.Delay(DelayTime).Wait();
            }
        }

        private void SimulateNextAiFrame()
        {
            while (true)
            {
                if (!simulation.Cars[0].Stopped)
                {
                    simulation.MakeNeuralMovement();
                    simulation.UpdateCollisions();
                }
                else
                {
                    simulation.Cars[0].Reset();
                }

                Task.Delay(DelayTime).Wait();
            }
        }

        private void SimulateNextTrainingFrame()
        {
            while (!tokenSource.IsCancellationRequested)
            {
                if (simulation.Cars.Any(c => !c.Stopped) && !forceEvolve)
                {
                    simulation.MakeNeuralMovement();
                    simulation.UpdateCollisions();
                    CheckForCompletion();
                }
                else
                {
                    geneticEngine.Evolve();
                    simulation.Cars.Clear();

                    for (int i = 0; i < geneticEngine.Population.Size; i++)
                    {
                        simulation.AddCar();
                        simulation.Cars[i].SetBrain(geneticEngine.Population.GetChromosomeByIndex(i));
                    }

                    simulation.Cars.ForEach(c => c.Reset());
                    simulation.Generation++;
                    forceEvolve = false;
                }

                Task.Delay(DelayTime).Wait();
            }
        }

        private void LogEvents()
        {
            while (true)
            {
                textboxLog.Dispatcher.Invoke(DispatcherPriority.Background, new Action(() => textboxLog.Text = simulation.ToString()));
            }
        }

        private void InitGeneticAlgorithm(int populationSize, int parentalChromosomesSurviveCount, CarProcessor baseNeuralNetwork)
        {
            var brains = new Population<CarProcessor>();

            for (int i = 0; i < populationSize - 1; i++)
            {
                var brain = baseNeuralNetwork == null ? CarProcessor.GetRandomProcessor() : baseNeuralNetwork.Mutate();
                brains.AddChromosome(brain);
            }

            var baseBrain = baseNeuralNetwork ?? CarProcessor.GetRandomProcessor();
            brains.AddChromosome(baseBrain);

            for (int i = 0; i < brains.Size; i++)
            {
                simulation.Cars[i].SetBrain(brains.GetChromosomeByIndex(i));
            }

            var fitness = new CarFitness(simulation);

            geneticEngine = new GeneticEngine<CarProcessor, double>(brains, fitness)
            {
                ParentChromosomesSurviveCount = parentalChromosomesSurviveCount
            };
        }

        private void buttonForceEvolve_Click(object sender, RoutedEventArgs e)
        {
            forceEvolve = true;
        }

        private void buttonSaveBrain_Click(object sender, RoutedEventArgs e)
        {
            if (!Directory.Exists(brainsDir.ToString()))
            {
                Directory.CreateDirectory(brainsDir.ToString());
            }

            EvolvingNetLoader.ToFile(simulation.BestCar.Brain, Path.Combine(brainsDir.ToString(), "CarProcessor.xml"));
        }

        private void buttonLoadBrain_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                Filter = "Brains|*.xml",
                InitialDirectory = Path.Combine(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), brainsDir.ToString())
            };

            openFileDialog.ShowDialog();
            var fName = openFileDialog.FileName;

            if (!string.IsNullOrEmpty(fName))
            {
                try
                {
                    loadedProcessor = EvolvingNetLoader.FromFile<CarProcessor>(fName);
                    labelBaseBrain.Content = $"Base brain:\n{fName.Substring(fName.LastIndexOf("\\") + 1)}";
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}