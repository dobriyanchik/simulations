﻿using MersenneTwister;
using SciML.GeneticAlgorithm;
using SciML.NeuralNetwork.Entities;
using SciML.NeuralNetwork.Evolution;
using SciML.NeuralNetwork.Evolution.Activation;
using System;
using System.Collections.Generic;

namespace RacingAI.NeuralNet
{
    public class CarProcessor : EvolvingNetBase, IChromosome<CarProcessor>
    {
        private const int InputsCount = Simulation.Car.SensorsCount;
        private const int HiddenCount = 5;
        private const int OutputsCount = 2;

        private readonly Random _randomizer = Randoms.FastestDouble;

        public CarProcessor() : base(InputsCount, HiddenCount, OutputsCount) { }

        public static CarProcessor GetRandomProcessor()
        {
            var randomBrain = new CarProcessor();
            randomBrain.ConstructNetwork();
            return randomBrain;
        }

        public List<CarProcessor> Crossover(CarProcessor anotherChromosome) =>
            base.Crossover(anotherChromosome);

        public CarProcessor Mutate() =>
            base.Mutate<CarProcessor>();

        public override void ConstructNetwork()
        {
            for (int i = 0; i < InputsCount; i++)
            {
                EvolvingActivationFunctionBase f = new LinearFunction();
                EvolvingNeuron n = new EvolvingNeuron(f);
                n.Inputs.Add(new Synapse(i, i, 1));
                InputLayer.Neurons[i] = n;
            }

            for (int i = 0; i < HiddenCount; i++)
            {
                EvolvingActivationFunctionBase f = EvolvingActivationFunctionBase.GetRandomFunction();
                EvolvingNeuron n = new EvolvingNeuron(f);
                HiddenLayer.Neurons[i] = n;
            }

            for (int i = 0; i < OutputsCount; i++)
            {
                EvolvingActivationFunctionBase f = EvolvingActivationFunctionBase.GetRandomFunction();
                EvolvingNeuron n = new EvolvingNeuron(f);
                n.Outputs.Add(new Synapse(i, i, 1));
                OutputLayer.Neurons[i] = n;
            }

            for (int i = 0; i < InputsCount; i++)
            {
                for (int j = 0; j < HiddenCount; j++)
                {
                    Connections[0].Add(new Synapse(i, j, _randomizer.NextDouble()));
                }
            }

            Connections[1].Add(new Synapse(0, 0, _randomizer.NextDouble()));
            Connections[1].Add(new Synapse(0, 1, _randomizer.NextDouble()));
            Connections[1].Add(new Synapse(1, 0, _randomizer.NextDouble()));
            Connections[1].Add(new Synapse(1, 1, _randomizer.NextDouble()));
            Connections[1].Add(new Synapse(2, 0, _randomizer.NextDouble()));
            Connections[1].Add(new Synapse(2, 1, _randomizer.NextDouble()));
            Connections[1].Add(new Synapse(3, 0, _randomizer.NextDouble()));
            Connections[1].Add(new Synapse(3, 1, _randomizer.NextDouble()));
            Connections[1].Add(new Synapse(4, 0, _randomizer.NextDouble()));
            Connections[1].Add(new Synapse(4, 1, _randomizer.NextDouble()));

            foreach (var synapse in Connections[0])
            {
                InputLayer.Neurons[synapse.InIndex].Outputs.Add(synapse);
                HiddenLayer.Neurons[synapse.OutIndex].Inputs.Add(synapse);
            }

            foreach (var synapse in Connections[1])
            {
                HiddenLayer.Neurons[synapse.InIndex].Outputs.Add(synapse);
                OutputLayer.Neurons[synapse.OutIndex].Inputs.Add(synapse);
            }
        }

        public override object Clone()
        {
            var clonedNetwork = new CarProcessor();

            for (int i = 0; i < InputsCount; i++)
            {
                clonedNetwork.InputLayer.Neurons[i] = InputLayer.Neurons[i].Clone() as EvolvingNeuron;
                clonedNetwork.InputLayer.Neurons[i].Inputs.Add(new Synapse(i, i, 1));
            }

            for (int i = 0; i < HiddenCount; i++)
            {
                clonedNetwork.HiddenLayer.Neurons[i] = HiddenLayer.Neurons[i].Clone() as EvolvingNeuron;
            }

            for (int i = 0; i < OutputsCount; i++)
            {
                clonedNetwork.OutputLayer.Neurons[i] = OutputLayer.Neurons[i].Clone() as EvolvingNeuron;
                clonedNetwork.OutputLayer.Neurons[i].Outputs.Add(new Synapse(i, i, 1));
            }

            Connections[0].ForEach(s => clonedNetwork.Connections[0].Add(s.Clone() as Synapse));
            Connections[1].ForEach(s => clonedNetwork.Connections[1].Add(s.Clone() as Synapse));

            foreach (var synapse in clonedNetwork.Connections[0])
            {
                clonedNetwork.InputLayer.Neurons[synapse.InIndex].Outputs.Add(synapse);
                clonedNetwork.HiddenLayer.Neurons[synapse.OutIndex].Inputs.Add(synapse);
            }

            foreach (var synapse in clonedNetwork.Connections[1])
            {
                clonedNetwork.HiddenLayer.Neurons[synapse.InIndex].Outputs.Add(synapse);
                clonedNetwork.OutputLayer.Neurons[synapse.OutIndex].Inputs.Add(synapse);
            }

            return clonedNetwork;
        }
    }
}
