﻿using RacingAI.Simulation;
using SciSim.Core.Physics;
using System;
using System.IO;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RacingAI.Visualization
{
    public class EvolutionVisualizer
    {
        private static readonly ImageBrush carSkin = new ImageBrush(new BitmapImage(new Uri(@"Resources\car.png", UriKind.Relative)));
        private static readonly ImageBrush carBrokenSkin = new ImageBrush(new BitmapImage(new Uri(@"Resources\carBroken.png", UriKind.Relative)));

        private readonly Brush _checkpointBrush = Brushes.Yellow;
        private readonly Brush _obstacleBrush = Brushes.Black;
        private readonly Brush _sensorGreen = Brushes.Green;
        private readonly Brush _sensorOrange = Brushes.Orange;
        private readonly Brush _sensorRed = Brushes.DarkRed;

        private readonly Canvas _canvas;
        private readonly RaceSimulation _environment;

        private readonly double _carSightDiameter = Car.SightRadius * 2;
        private readonly DoubleCollection _strokeDashArray = new DoubleCollection() { 3, 9 };

        public EvolutionVisualizer(RaceSimulation environment, Canvas trackCanvas, Canvas canvas)
        {
            _canvas = canvas;
            _environment = environment;
            trackCanvas.Children.Clear();

            var trackImageUri = new Uri($@"Resources\Tracks\{environment.Track.Name}.png", UriKind.Relative);

            if (File.Exists(trackImageUri.ToString()))
            {
                var trackImage = new BitmapImage(trackImageUri);
                trackCanvas.Height = trackImage.Height;
                trackCanvas.Width = trackImage.Width;
                canvas.Height = trackImage.Height;
                canvas.Width = trackImage.Width;
                trackCanvas.Background = new ImageBrush(trackImage);
            }
            else
            {
                trackCanvas.Height = 712;
                trackCanvas.Width = 1024;
                canvas.Height = 712;
                canvas.Width = 1024;
                trackCanvas.Background = Brushes.White;
            }

            if (!File.Exists(trackImageUri.ToString()))
            {
                _environment.Track.Borders.ForEach(b => InitializeObstacle(b, trackCanvas));
            }

            _environment.Track.Obstacles.ForEach(o => InitializeObstacle(o, trackCanvas));
        }

        public void DrawNextFrame()
        {
            _canvas.Children.Clear();

            for (int i = _environment.BestCar.CheckpointsPassed; i < _environment.Track.Checkpoints.Count; i++)
            {
                var checkpoint = _environment.Track.Checkpoints[i];

                Line checkpointLine = new Line
                {
                    X1 = checkpoint.V1.X,
                    Y1 = checkpoint.V1.Y,
                    X2 = checkpoint.V2.X,
                    Y2 = checkpoint.V2.Y,
                    Stroke = _checkpointBrush,
                    StrokeThickness = 1
                };

                _canvas.Children.Add(checkpointLine);
            }

            _environment.Cars.ForEach(c =>
                DrawCar(c, c.CheckpointsPassed.Equals(_environment.BestCar.CheckpointsPassed)));
        }

        private void DrawCar(Car car, bool best)
        {
            if (!car.Stopped)
            {
                foreach (var sensor in car.Sensors)
                {
                    var line = new Line
                    {
                        Stroke = GetSensorColor(sensor.Geometry.Collision.Severity),
                        StrokeThickness = 1,
                        X1 = sensor.Geometry.V1.X,
                        Y1 = sensor.Geometry.V1.Y,
                        X2 = sensor.Geometry.V2.X,
                        Y2 = sensor.Geometry.V2.Y,
                    };

                    _canvas.Children.Add(line);

                    if (sensor.Geometry.Collision.Severity > 0)
                    {
                        var collisionEllipse = new Ellipse
                        {
                            Stroke = Brushes.Red,
                            StrokeThickness = 3,
                            Width = 6,
                            Height = 6,
                        };

                        Canvas.SetLeft(collisionEllipse, sensor.Geometry.Collision.Location.X - 3);
                        Canvas.SetTop(collisionEllipse, sensor.Geometry.Collision.Location.Y - 3);

                        _canvas.Children.Add(collisionEllipse);
                    }
                }
            }


            var carRectangle = new Rectangle
            {
                Height = Car.Height,
                Width = Car.Width,
                Fill = car.Stopped ? carBrokenSkin : carSkin
            };

            RotateTransform rotation = new RotateTransform(car.Angle, Car.HalfWidth, Car.HalfHeight);
            TranslateTransform move = new TranslateTransform(car.Location.X - Car.HalfWidth, car.Location.Y - Car.HalfHeight);

            TransformGroup transforms = new TransformGroup();
            transforms.Children.Add(rotation);
            transforms.Children.Add(move);

            carRectangle.RenderTransform = transforms;

            _canvas.Children.Add(carRectangle);

            if (best)
            {
                var ellipseInSight = new Ellipse
                {
                    Stroke = Brushes.Gray,
                    StrokeThickness = 1,
                    StrokeDashArray = _strokeDashArray,
                    Width = _carSightDiameter,
                    Height = _carSightDiameter,
                };

                Canvas.SetLeft(ellipseInSight, car.Location.X - Car.SightRadius);
                Canvas.SetTop(ellipseInSight, car.Location.Y - Car.SightRadius);

                _canvas.Children.Add(ellipseInSight);
            }
        }

        private void InitializeObstacle(CollidingSegment obstacle, Canvas trackCanvas)
        {
            Line obstacleLine = new Line
            {
                X1 = obstacle.V1.X,
                Y1 = obstacle.V1.Y,
                X2 = obstacle.V2.X,
                Y2 = obstacle.V2.Y,
                Stroke = _obstacleBrush,
                StrokeThickness = 1
            };

            trackCanvas.Children.Add(obstacleLine);
        }

        private Brush GetSensorColor(double coefficient)
        {
            if (coefficient > 0.5)
            {
                return _sensorRed;
            }
            else if (coefficient > 0)
            {
                return _sensorOrange;
            }
            else
            {
                return _sensorGreen;
            }
        }
    }
}
