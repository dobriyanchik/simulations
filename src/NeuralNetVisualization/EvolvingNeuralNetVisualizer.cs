﻿using SciML.NeuralNetwork.Evolution;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SciSim.NNVisualization
{
    public class EvolvingNeuralNetVisualizer
    {
        private readonly Canvas _canvas;

        private readonly Brush brushInactiveNeuron = Brushes.Transparent;
        //private readonly Brush brushBackground = Brushes.WhiteSmoke;
        private readonly Brush brushNeuronMain = Brushes.Crimson;
        private readonly Brush brushActiveNeuron = Brushes.Orange;
        private readonly Brush brushSynapse = Brushes.OrangeRed;

        private double neuronSize;
        private double maxSinapseThickness;
        private double yDistance;

        private double xCenter1;
        private double xCenter2;
        private double xCenter3;
        private double yOffset1;
        private double yOffset2;
        private double yOffset3;

        private readonly Size _size;

        public EvolvingNeuralNetVisualizer(Canvas canvas)
        {
            _canvas = canvas;
            //this.canvas.Background = brushBackground;
            neuronSize = 0;
            _size = new Size(canvas.ActualWidth, canvas.ActualHeight);
        }

        public void DrawBrain(EvolvingNetBase brain)
        {
            _canvas.Children.Clear();

            int inputsCount = brain.InputLayer.Neurons.Length;
            int hiddenCount = brain.HiddenLayer.Neurons.Length;
            int outputsCount = brain.OutputLayer.Neurons.Length;

            if (neuronSize == 0)
            {
                CalculateSizes(inputsCount, hiddenCount, outputsCount);
            }

            var maxSynapseValue = brain.Connections[0].Max(s => s.Signal);
            var minSynapseValue = brain.Connections[0].Min(s => s.Signal);

            foreach (var synapse in brain.Connections[0])
            {
                var sourceCenter = GetItemCenter(synapse.InIndex, yOffset1, xCenter1);
                var destinationCenter = GetItemCenter(synapse.OutIndex, yOffset2, xCenter2);

                DrawSynapse(sourceCenter, destinationCenter, GetSynapseThickness(synapse.Signal, minSynapseValue, maxSynapseValue));
            }

            maxSynapseValue = brain.Connections[1].Max(s => s.Signal);
            minSynapseValue = brain.Connections[1].Min(s => s.Signal);

            foreach (var synapse in brain.Connections[1])
            {
                var sourceCenter = GetItemCenter(synapse.InIndex, yOffset2, xCenter2);
                var destinationCenter = GetItemCenter(synapse.OutIndex, yOffset3, xCenter3);

                DrawSynapse(sourceCenter, destinationCenter, GetSynapseThickness(synapse.Signal, minSynapseValue, maxSynapseValue));
            }

            maxSynapseValue = brain.InputLayer.Neurons.Select(n => n.Inputs[0]).Max(s => s.Signal);
            minSynapseValue = brain.InputLayer.Neurons.Select(n => n.Inputs[0]).Min(s => s.Signal);

            for (int i = 0; i < inputsCount; i++)
            {
                var sourceCenter = GetItemCenter(i, yOffset1, xCenter1);
                var pointStart = new Point(0, sourceCenter.Y);
                var thickness = GetSynapseThickness(brain.InputLayer.Neurons[i].Inputs[0].Signal, minSynapseValue, maxSynapseValue);

                DrawSynapse(pointStart, sourceCenter, thickness);
                DrawNeuron(brain.InputLayer.Neurons[i], sourceCenter);
            }

            for (int i = 0; i < hiddenCount; i++)
            {
                var sourceCenter = GetItemCenter(i, yOffset2, xCenter2);

                DrawNeuron(brain.HiddenLayer.Neurons[i], sourceCenter);
            }

            maxSynapseValue = brain.OutputLayer.Neurons.Select(n => n.Outputs[0]).Max(s => s.Signal);
            minSynapseValue = brain.OutputLayer.Neurons.Select(n => n.Outputs[0]).Min(s => s.Signal);

            for (int i = 0; i < outputsCount; i++)
            {
                var sourceCenter = GetItemCenter(i, yOffset3, xCenter3);
                var pointStart = new Point(_size.Width, sourceCenter.Y);
                var thickness = GetSynapseThickness(brain.OutputLayer.Neurons[i].Outputs[0].Signal, minSynapseValue, maxSynapseValue);

                DrawSynapse(pointStart, sourceCenter, thickness);
                DrawNeuron(brain.OutputLayer.Neurons[i], sourceCenter);
            }
        }

        private void DrawNeuron(EvolvingNeuron neuron, Point center)
        {
            var ellipse = new Ellipse
            {
                Fill = brushNeuronMain,
                Width = neuronSize,
                Height = neuronSize,
                Stroke = GetNeuronColor(neuron),
                StrokeThickness = 3
            };

            Canvas.SetLeft(ellipse, center.X - neuronSize / 2);
            Canvas.SetTop(ellipse, center.Y - neuronSize / 2);
            _canvas.Children.Add(ellipse);
        }

        private void DrawSynapse(Point start, Point end, double thickness)
        {
            Line line = new Line
            {
                X1 = start.X,
                Y1 = start.Y,
                X2 = end.X,
                Y2 = end.Y,
                Stroke = brushSynapse,
                StrokeThickness = thickness
            };

            _canvas.Children.Add(line);
        }

        /// <summary>
        /// Get Entity center coordinates
        /// </summary>
        /// <param name="index">index of entity in list</param>
        /// <param name="items">count of entities in current list</param>
        /// <param name="maxItems">count of entities in the longest list</param>
        /// <param name="itemSize">calculated single item size</param>
        /// <param name="xOffset">offset by X coordinate (based on entity type)</param>
        /// <returns></returns>
        private Point GetItemCenter(int index, double yOffset, double xCenter) =>
            new Point(xCenter, yOffset * (index + 1));

        /// <summary>
        /// Calculate entity size based on count of entities in list and specified imege height
        /// </summary>
        /// <param name="height">image height</param>
        /// <param name="maxLayerItemsCount">count of entities in list</param>
        /// <returns></returns>
        private void CalculateSizes(int inputsCount, int hiddenCount, int outputsCount)
        {
            int maxLayerItemsCount = Math.Max(Math.Max(inputsCount, hiddenCount), outputsCount);

            neuronSize = Math.Min(_size.Height / (maxLayerItemsCount * 2 - 1), _size.Width / 7d);
            maxSinapseThickness = neuronSize / 2d;
            var xDistance = _size.Width / 3d;
            yDistance = (_canvas.ActualHeight - maxLayerItemsCount * neuronSize) / (maxLayerItemsCount - 1);

            xCenter1 = 0.5 * xDistance;
            xCenter2 = 1.5 * xDistance;
            xCenter3 = 2.5 * xDistance;

            yOffset1 = _size.Height / (inputsCount + 1);
            yOffset2 = _size.Height / (hiddenCount + 1);
            yOffset3 = _size.Height / (outputsCount + 1);
        }

        /// <summary>
        /// Get neuron color based on sign of output signal
        /// </summary>
        /// <param name="neuron">current neuron instance</param>
        /// <returns>colored brush</returns>
        private Brush GetNeuronColor(EvolvingNeuron neuron) =>
            neuron.Outputs[0].Signal > 0 ? brushActiveNeuron : brushInactiveNeuron;

        private double GetSynapseThickness(double current, double minValue, double maxValue) =>
            (current - minValue) * (maxSinapseThickness / (maxValue - minValue)) + 0.2;
    }
}
