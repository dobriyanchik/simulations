﻿using System.Windows.Media.Media3D;

namespace SciSim.Core.Graphics
{
    public class Modificator
    {
        private readonly Transform3DGroup _transformGroup;

        public Modificator()
        {
            _transformGroup = new Transform3DGroup();
        }

        public Modificator Scale(double scaleX, double scaleY, double scaleZ)
        {
            var scaleTransform = new ScaleTransform3D
            {
                ScaleX = scaleX,
                ScaleY = scaleY,
                ScaleZ = scaleZ
            };

            _transformGroup.Children.Add(scaleTransform);

            return this;
        }

        public Modificator Move(double x, double y, double z)
        {
            var translateTransform = new TranslateTransform3D
            {
                OffsetX = x,
                OffsetY = y,
                OffsetZ = z
            };

            _transformGroup.Children.Add(translateTransform);

            return this;
        }

        public Modificator Rotate(Vector3D axis, double angle, Point3D center)
        {
            var rotateTransform = new RotateTransform3D
            {
                Rotation = new AxisAngleRotation3D
                {
                    Axis = axis,
                    Angle = angle
                },
                CenterX = center.X,
                CenterY = center.Y,
                CenterZ = center.Z
            };

            _transformGroup.Children.Add(rotateTransform);

            return this;
        }

        public void ApplyTo(Model3D model) =>
            model.Transform = _transformGroup;
    }
}
