﻿using System.Windows.Media.Media3D;

namespace SciSim.Core.Graphics
{
    public interface IModelCreator
    {
        GeometryModel3D CreateModel(Material material);
    }
}
