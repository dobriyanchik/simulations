﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace SciSim.Core.Graphics
{
    public class SurfaceCreator
    {
        public GeometryModel3D CreateSurface(int[,] heightMap, Material material)
        {
            var mesh = new MeshGeometry3D();

            DefineModel(mesh, heightMap);

            return new GeometryModel3D(mesh, material)
            {
                BackMaterial = material
            };
        }

        private void DefineModel(MeshGeometry3D mesh, int[,] map)
        {
            for (double x = 0; x < map.GetLength(0); x++)
            {
                for (double y = 0; y < map.GetLength(1); y++)
                {
                    Point3D p00 = new Point3D(x, y, GetHeight(x, y) / 2 + GetHeight(x - 1, y - 1) / 2);
                    Point3D p10 = new Point3D(x + 1, y, GetHeight(x + 1, y) / 2 + GetHeight(x, y - 1) / 2);
                    Point3D p01 = new Point3D(x, y + 1, GetHeight(x, y + 1) / 2 + GetHeight(x - 1, y) / 2);
                    Point3D p11 = new Point3D(x + 1, y + 1, GetHeight(x + 1, y + 1) / 2 + GetHeight(x, y) / 2);

                    AddTriangle(mesh, p00, p01, p11);
                    AddTriangle(mesh, p00, p11, p10);
                }

                double GetHeight(double px, double py)
                {
                    var newX = System.Math.Max(System.Math.Min(map.GetLength(0) - 1, (int)px), 0);
                    var newY = System.Math.Max(System.Math.Min(map.GetLength(1) - 1, (int)py), 0);

                    return map[newX, newY];
                }
            }
        }

        private void AddTriangle(MeshGeometry3D mesh, Point3D p1, Point3D p2, Point3D p3)
        {
            int index1 = AddPoint(mesh.Positions, mesh.TextureCoordinates, p1);
            int index2 = AddPoint(mesh.Positions, mesh.TextureCoordinates, p2);
            int index3 = AddPoint(mesh.Positions, mesh.TextureCoordinates, p3);

            mesh.TriangleIndices.Add(index1);
            mesh.TriangleIndices.Add(index2);
            mesh.TriangleIndices.Add(index3);
        }

        //private Dictionary<Point3D, int> pts = new Dictionary<Point3D, int>();

        private int AddPoint(Point3DCollection points, PointCollection textureCoords, Point3D point)
        {
            //if (pts.ContainsKey(point))
            //{
            //    return pts[point];
            //}

            points.Add(point);
            //pts.Add(point, points.Count - 1);

            textureCoords.Add(new Point(point.X, point.Y));

            return points.Count - 1;
        }

    }
}
